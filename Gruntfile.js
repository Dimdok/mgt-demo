'use strict';

module.exports = function(grunt) {

  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.loadNpmTasks('assemble');

  // configure the tasks
  grunt.initConfig({

    clean: {
      build: {
        src: ['build']
      }
    },

    copy: {

      assets: {
        expand: true,
        cwd: 'source',
        src: [
          '**/*.{png,gif,jpg,eot,svg,ttf,woff,woff2,mp3,ogg,json}',
          '!stylesheets/**',
          '!images/sprite/**'
        ],
        dest: 'build'
      },

      javascripts: {
        expand: true,
        flatten: true,
        src: [
          'source/javascripts/vendor/modernizr-custom.js'
        ],
        dest: 'build/javascripts/vendor'
      }

    },

    assemble: {
      options: {
        helpers: [
          'handlebars-helpers',
          'handlebars-helper-repeat'
        ],
        partials: 'source/templates/partials/**/*.hbs',
        layoutdir: 'source/templates/layouts',
        ext: '.html',
        data: 'bower.json'
      },
      site: {
        expand: true,
        cwd: 'source/templates/pages',
        src: ['**/*.hbs'],
        dest: 'build'
      }
    },

    sprite:{
      all: {
        src: [
          'source/stylesheets/blocks/**/*.png',
          'source/images/sprite/*.png'
        ],
        cssTemplate: 'source/stylesheets/less.template.handlebars',
        destCss: 'temp/stylesheets/sprite.less',
        dest: 'build/images/sprite.png',
        imgPath: '../images/sprite.png',
        cssVarMap: function(sprite) {

          var varName,

            basePaths = [
                'source/stylesheets/',
                'source/images/'
               ],

            fullPath = sprite.source_image.replace(/\\/g, '/');

          for (var i = 0; i < basePaths.length; i++) {
            varName = fullPath.split(basePaths[i])[1];
            if (varName) break;
          }

          if (varName) {
            varName = varName.replace(/\//g, '-')
              .replace(/(.+)\..+$/, '$1')
              .replace(/^sprite-(.+)/, 'images-$1');

            sprite.name = 'icon-' + varName;
          }
        }
      }
    },

    less: {
      build: {
        options: {
          compress: true
        },
        expand: true,
        cwd: 'source/stylesheets',
        src: [ '*.less', '!_*.less' ],
        //dest: 'temp/stylesheets',
        dest: 'build/stylesheets',
        ext: '.css'
      }
    },

    autoprefixer: {
      options: {
        browsers: [
          'last 2 versions', 'ie >= 9',
          'Opera >= 12.0', 'Chrome >= 9.0',
          'ff >= 5.0', 'Safari >= 5.0'
        ]
      },
      build: {
        ///src: 'temp/stylesheets/*.css'
        src: 'build/stylesheets/*.css'
      }
    },

    uglify: {
      build: {
        files: {
          'build/javascripts/libs.js': [
            'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
            'bower_components/lightslider/lightSlider/js/jquery.lightSlider.js',
            'bower_components/fotorama/fotorama.js',
            'bower_components/bxslider-4/dist/jquery.bxslider.js',
            'bower_components/jquery-mousewheel/jquery.mousewheel.js',
            'bower_components/fancybox/source/jquery.fancybox.js',
            'bower_components/bootstrap-less/js/tab.js',
            'bower_components/bootstrap-less/js/tooltip.js',
            'bower_components/bootstrap-less/js/dropdown.js',
            'bower_components/bootstrap-less/js/modal.js',
            'bower_components/bootstrap-less/js/popover.js',
            'bower_components/bootstrap-less/js/carousel.js',
            'bower_components/bootstrap-less/js/transition.js',
            'bower_components/bootstrap-less/js/collapse.js',
            'bower_components/social-likes/social-likes.min.js',
            'source/javascripts/vendor/jquery.gray-custom.js',

            // form specific
            'bower_components/jquery-form/jquery.form.js',
            'bower_components/select2/dist/js/select2.js',
            'bower_components/select2/dist/js/i18n/ru.js',
            'bower_components/jquery-ui/ui/i18n/datepicker-ru.js',
            'bower_components/placeholders/dist/placeholders.jquery.js',
            'bower_components/jquery-autosize/dist/autosize.js',
            'bower_components/jquery-validation/dist/jquery.validate.js',
            'source/javascripts/vendor/jquery-validation-messages_ru.js',
            'bower_components/bootstrap-filestyle/src/bootstrap-filestyle.js',
            'bower_components/tooltipster/js/jquery.tooltipster.js',
            'bower_components/iCheck/icheck.js',

            'bower_components/raty/lib/jquery.raty.js'
          ]
        }
      }
    },

    concat: {
      javascripts: {
        files: {
          'build/javascripts/app.js': [
            'source/javascripts/**/*.js',
			      '!source/javascripts/app-site.js',
			      '!source/javascripts/app-add-site.js',
            '!source/javascripts/vendor/**'
          ],
			      'build/javascripts/app-site.js': 'source/javascripts/app-site.js',
			      'build/javascripts/app-add-site.js': 'source/javascripts/app-add-site.js'
        }
      }
    },

    watch: {
      options: {
        livereload: true
      },
      templates: {
        files: 'source/templates/**',
        tasks: 'assemble'
      },
      assets: {
        files: [
          'source/**/*.{png,gif,jpg,eot,svg,ttf,woff,woff2,mp3,ogg,json}',
          '!source/stylesheets/**',
          '!source/images/sprite/**'
        ],
        tasks: 'copy:assets'
      },
      stylesheets: {
        files: [
          'source/stylesheets/**',
          'source/images/sprite/**'
        ],
        tasks: 'stylesheets'
      },
      javascripts: {
        files: 'source/javascripts/**',
        tasks: 'javascripts'
      },
      gruntfile: {
        files: 'Gruntfile.js',
        tasks: 'build'
      }
    },

    connect: {
      server: {
        options: {
          base: 'build',
          livereload: true,
          open: true,
		      port: 8888
        }
      }
    }
  });

  // define the tasks
  grunt.registerTask('stylesheets', [ 'sprite', 'less', 'autoprefixer' ]);

  grunt.registerTask('javascripts', [
    'copy:javascripts',
    'concat:javascripts',
    'uglify'
  ]);

  grunt.registerTask('build', [
    'clean',
    'copy:assets',
    'stylesheets',
    'javascripts',
    'assemble'
  ]);


  grunt.registerTask('default', ['build','connect','watch']);
  //grunt.registerTask('serve', [ 'build', 'connect', 'watch' ]);
  grunt.registerTask('deploy', [ 'rsync' ]);
  grunt.registerTask('deploy-sb', ['build', 'rsync:sb0' ]);


};
