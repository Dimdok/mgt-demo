/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

jQuery(function($) {

	/**
	 * init slider desc
	 */

	(function () {
		var $slider = $('#slider-desc');

		$('.slider__body', $slider).bxSlider({
			nextSelector: $('.slider__next', $slider),
			pagerSelector: $('.slider__pager', $slider),
			prevSelector: $('.slider__prev', $slider),
			prevText: '',
			nextText: ''
		});
	}());

	/**
	 * init slider thumbs
	 */

	(function () {

		var navTemplate = '<div class="slider-thumbs__nav">' +
				'<div class="slider-thumbs__nav__tube">' +
					'<% _.each(images, function(image, index) { %>' +
						'<% if (index == 0) { %>' +
							'<a href="#" class="slider-thumbs__nav__item slider-thumbs__nav__item_active">' +
								'<span class="slider-thumbs__nav__item__in">' +
									'<img src="<%= image.src %>" class="slider-thumbs__nav__image">' +
								'</span>' +
							'</a>' +
						'<% } else { %>' +
							'<a href="#" class="slider-thumbs__nav__item">' +
								'<span class="slider-thumbs__nav__item__in">' +
									'<img src="<%= image.src %>" class="slider-thumbs__nav__image">' +
								'</span>' +
							'</a>' +
						'<% } %>' +
					'<% }); %>' +
				'</div>' +
			'</div>' +
			'<a href="#" class="slider-thumbs__menu"></a>';

		var $sliders = $('.slider-thumbs');

		$sliders.each(function() {

			var $slider = $(this);
			var uniqId = Math.random().toString(36).substring(7);
			var $images = $slider.find('img');
			var navData = {images: []};

			// fil navData

			$images.each(function() {
				navData.images.push({ src: $(this).attr('src') });
			});

			// create structure

			$slider.wrapInner('<div class="slider-thumbs__body"></div>');
			$slider.find('img').each(function() {
				var src = $(this).attr('src');

				$(this).wrap('<a href="' + src +
					'" data-fancybox-group="' +
					uniqId +'"></a>');
			});
			$slider.find('a').wrap('<div class="slider-thumbs__item"></div>');

			var navHtml = _.template(navTemplate)(navData);
			$(navHtml).prependTo($slider);

			// init slider

			var bxSlider = $slider.find('.slider-thumbs__body').bxSlider({
				pager: false,
				controls: false,
				adaptiveHeight: true,
				captions: true,
				mode: 'fade'
			});

			$slider.data('bxSlider', bxSlider);

			if($(this).hasClass('slider-width-title')){
				$images.each(function(i){
					$slider.find('.slider-thumbs__item').eq(i).append('<div class="promo__gallery-title">'+$(this).data('title')+'</div>');
				})
			}
		});

		$(document).on('click', '.slider-thumbs__menu', openFancybox);
		$(document).on('click', '.slider-thumbs__nav__item', switchSlide);
		$sliders.find('a[data-fancybox-group]').fancybox();

		// try keep active item in middle
		function fixNavScroll($slider) {

			var menuHeight = 60;
			var $box = $slider.find('.slider-thumbs__nav');
			var $tube = $box.find('.slider-thumbs__nav__tube');
			var boxHeight = $box.height() - menuHeight;
			var tubeHeight = $tube.height();

			if (boxHeight >= tubeHeight) return;

			var $items = $box.find('.slider-thumbs__nav__item');
			var $active = $items.filter('.slider-thumbs__nav__item_active');
			var index = $active.index();
			var itemHeight = $items.height();
			var itemsGap = parseInt($items.css('margin-bottom'));
			var activePos = Math.ceil(boxHeight/(itemHeight + itemsGap)/2);

			var maxOffset = tubeHeight - boxHeight;
			var minOffset = 0;

			var offset = (index+1-activePos)*(itemHeight+itemsGap);
			if (offset > maxOffset) offset = maxOffset;
			else if (offset < minOffset) offset = minOffset;

			$tube.stop(true).animate({
				marginTop: -offset
			}, 'fast');

		}

		function openFancybox(e) {
			e.preventDefault();

			$(this)
				.closest('.slider-thumbs')
				.find('.slider-thumbs__item:visible a')
				.trigger('click');
		}

		function switchSlide(e) {
			e.preventDefault();

			var $slider = $(this).closest('.slider-thumbs');
			var bxSlider = $slider.data('bxSlider');
			var index = $(this).index();

			$(this)
				.siblings('.slider-thumbs__nav__item_active')
				.removeClass('slider-thumbs__nav__item_active')
				.end()
				.addClass('slider-thumbs__nav__item_active');

			bxSlider.goToSlide(index);
			fixNavScroll($slider);
		}

	}());

	/**
	 * init nav tour
	 */

	(function () {

		var slider,
			$slider = $('#nav-tour .nav-tour__in'),
			$activeItem = $slider.find('.nav-tour__link.active').closest('.nav-tour__item'),
			targetIndex = ($activeItem.size()) ? $activeItem.index() : 0,
			count = $slider.find('.nav-tour__item').size(),
			visible = 6,
			maxStart = count - visible,
			start = targetIndex - 1;

		if (!$slider.size() || count <= visible) return;

		if (start < 0) start = 0;
		else if (start > maxStart) start = maxStart;

		slider = $slider.lightSlider({
			item: visible,
			slideMargin: 0,
			useCSS: false,
			pager: false
		});

		slider.goToSlide(start);

	}());


	/**
	 * init faq collapsing
	 */

	(function () {

		$('.js-faq-collapse').on('show.bs.collapse hide.bs.collapse', function() {
			$(this).closest('.faq').toggleClass('faq_open');
		});

	}());

	/**
	 * init raty
	 */

	(function () {

		$('.js-raty').raty({
			space: false,
			starOff: 'images/star-off.png',
			starOn: 'images/star-on.png',
			scoreName: function() {
				return $(this).data('score-name');
			}
		});

	}());

	/**
	 * init fancybox
	 */

	(function () {

		$('.js-fancybox').fancybox();

	}());

	/**
	 * init popover
	 */

	(function () {

		$('.js-popover').popover();

		$(document).on('click', '.popover-close', function(e) {
			e.preventDefault();
			$(this).closest('.popover').popover('hide');
		});

		$(document).on('click', '.js-popover', function(e) {
			e.preventDefault();
		});

	}());


	/**
	 * init datetimepicker
	 */

	(function() {

		// [Block for each individual calendar]
		var $parent = $(".js-datetimepicker")
			uniqIdFromTo = 0,
			uniqIdFrom = 0,
			uniqIdTo = 0; // Unique id for from/to calendar diapason

		// If Archive href
		if($parent.hasClass('archive')) {
			$parent.find('form').addClass('archive__form').removeClass('dnone').find('.btn').removeClass('dnone');
			$parent.on('click', function(e) {
				e.preventDefault();
				$parent.addClass('open');
			});
			$parent.find('.btn').on('click', function(e) {
				e.preventDefault();

				return false;
			});
		}

		// [The format of the dates displayed in the input field]
		$.datepicker._defaults.dateFormat = 'dd.mm.yy' ;

		// [Update the design of the selectors in the calendar and add Close button]
		var timePickerCustomSelect = function(obj) {
			if( obj.find('.select-wrapper').size() == 0 ) {
				obj.find('select').wrap("<div class='select-wrapper'></div>");
				$("<a class='close-datepicker' href='#'></a>").insertAfter( obj.find('.select-wrapper:last') );
			}
			return true;
		}

		var createSimpleCalendar = function(params) {

			var uniqIdFromTo = (typeof params.uniqIdFromTo !== 'undefined')? params.uniqIdFromTo : 0 ;
			var indxCal = params.indxCal;

			if(typeof params.daterange1 !== 'undefined' && typeof params.daterange2 !== 'undefined') {
				params.$calendar.datepicker({
					autoclose: false,
					changeMonth: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					changeYear: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					yearRange : '1910:2050',
					minDate : (params.minDate)? params.minDate : null,
					maxDate : (params.maxDate)? params.maxDate : null,
					defaultDate : null,
					beforeShowDay: function(date) {
						var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange1.val());
						var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange2.val());
						return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
					},
					onSelect: function(dateText, inst) {
						var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange1.val());
						var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange2.val());
						if (!date1 || date2) {
							params.daterange1.val(dateText);
							params.daterange2.val("");
							$(this).datepicker("option", "minDate", dateText);
						} else {
							params.daterange2.val(dateText);
							$(this).datepicker("option", "minDate", null);
						}

						if(params.daterange2.val() != '' && params.daterange1.val() != '') {
							var changeDateEvent = $.Event('change.dbl.date');
							$(this).trigger(changeDateEvent);
						}

						// [If both are selected in the calendar with the date range, you can close it]
						params.form_control.val( (params.daterange2.val() != '' && params.daterange1.val() != '')? params.daterange1.val() + " - " + params.daterange2.val() : params.daterange1.val() );

						setTimeout(function() {
							params.$calendar.find("td.dp-highlight:eq(0)>a").addClass("ui-state-active");
							timePickerCustomSelect(params.$calendar);
						}, 5);
					},
					onClose:function(selectedDate) {
						$.datepicker._datepickerShowing = false;
					},
					onChangeMonthYear: function(year, month, inst) {
						// Update the design of the selectors in the calendar
						setTimeout(function() {
							timePickerCustomSelect(params.$calendar);
						}, 5);
					}
				});

			} else {

				// [Calendar one date for select year/month if exist class js-datetimepicker-born]
				params.$calendar.datepicker({
					autoclose: false,
					changeMonth: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					changeYear: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					yearRange : '1910:2050',
					minDate : (params.minDate)? params.minDate : null,
					maxDate : (params.maxDate)? params.maxDate : null,
					defaultDate : (params.minDate && uniqIdFromTo && uniqIdFromTo.indexOf("to") >= 0)? params.minDate : (params.maxDate && uniqIdFromTo && uniqIdFromTo.indexOf("from") >= 0)? params.maxDate : null,
					onSelect: function(dateText, inst) {
						if(typeof params.form_control != 'undefined') {
							params.form_control.val(dateText);
						}
						// Update the design of the selectors in the calendar
						setTimeout(function() {
							timePickerCustomSelect(params.$calendar);
						}, 5);

						var countsNight = 1;

						if(params.minDate && params.maxDate) {
							var countsNight = (new Date(params.maxDate).getTime() - new Date(params.minDate).getTime()) / 86400000;
						}

						if(uniqIdFromTo) {
							var minMaxDate = (uniqIdFromTo.indexOf("from") >= 0)? "maxDate" : "minDate";

							var date_arr = dateText.split('.'),
								toDate = new Date(Date.parse(date_arr[2] + '.' + date_arr[1] + '.' + date_arr[0] )),
								oneDay = (minMaxDate == 'maxDate')? new Date(toDate.getTime() - 86400000) : new Date(toDate.getTime() + 86400000);

							$("#" + uniqIdFromTo)
												.datepicker( "option", minMaxDate, oneDay );

							if(minMaxDate == 'minDate') {
								countsNight = ($("#" + uniqIdFromTo).datepicker( "getDate" ).getTime() - $("#" + uniqIdFromTo.replace('to','from')).datepicker( "getDate" ).getTime()) / 86400000;
							} else {
								countsNight = ($("#" + uniqIdFromTo.replace('from','to')).datepicker( "getDate" ).getTime() - $("#" + uniqIdFromTo).datepicker( "getDate" ).getTime()) / 86400000;
							}

							// Add change selected range date
							var $nightCount = $('#' + params.form_control.data("night-count-target")),
								$nightPrice = $('#' + params.form_control.data("night-price-target"));

							if($nightCount.size()) {
								if($nightCount.is("select")) {
									if($nightCount.find("option[value='" + countsNight  + "']").size() > 0) {
										$nightCount
											.find("option")
											.prop('selected', false)
											.removeAttr('selected')
											.filter("[value='" + countsNight  + "']")
											.attr('selected','selected')
											.prop('selected', true);
									} else {
										$nightCount
											.find("option")
											.removeAttr('selected')
											.prop('selected', false);
										$nightCount
											.append($("<option></option>")
											.attr('selected','selected')
											.prop('selected', true)
											.attr("value",countsNight)
											.text(countsNight));

									};
								} else {
									$nightCount.val(countsNight);
								}
							}

							if($nightPrice.size() && $nightPrice.data('base-price')) {
								var price = $nightPrice.data('base-price') * countsNight;
								$nightPrice.html( price.format(0, 3, ' ', '.') );
							}

						}

					},
					onClose:function(selectedDate) {
						$.datepicker._datepickerShowing = false;
					},
					onChangeMonthYear: function(year, month, inst) {
						// Update the design of the selectors in the calendar
						setTimeout(function() {
							timePickerCustomSelect(params.$calendar);
						}, 5);
					}
				});


				// SetDefault Nights count
				var $nightCount = $('#' + params.form_control.data("night-count-target")),
					$nightPrice = $('#' + params.form_control.data("night-price-target"));

				if(params.minDate && params.maxDate && $nightCount.size()) {
					var arr_min = params.minDate.split('.'),
						toDateMin = new Date(Date.parse(arr_min[2] + '-' + arr_min[1] + '-' + arr_min[0] )).getTime(),
						arr_max = params.maxDate.split('.'),
						toDateMax = new Date(Date.parse(arr_max[2] + '-' + arr_max[1] + '-' + arr_max[0] )).getTime();
					var countsNight = (toDateMax - toDateMin) / 86400000;
					$nightCount.val(countsNight);
					var price = $nightPrice.data('base-price') * countsNight;
					$nightPrice.html( price.format(0, 3, ' ', '.') );

				}



			}
		}

		var calendarsCycle = function(params) {

			var $calendar = $(".js-datetimepicker-calendar", params.$this), // span, into calendar parse
				daterange1 = $(".js-datetimepicker-date-1", params.$this), // Hidden field for first-date
				daterange2 = $(".js-datetimepicker-date-2", params.$this), // Hidden field for two-date, for dates diapazon
				form_control = $(".js-datetimepicker-input", params.$this), // Input field, when you click on the calendar that displays
				form_control_add = $(".js-datetimepicker-icon", params.$this), // Calendar icon, when you click on the calendar that displays
				uniqId = Math.random().toString(36).substring(7), // Unique ID
				minDate = null,
				maxDate = null;

				if( params.$this.hasClass('js-datetimepicker-from') ) {
					uniqIdFromTo = uniqId;
					params.$this
						.find('.js-datetimepicker-calendar')
						.attr('id', uniqIdFromTo + '-from')
					params.$this
						.find('input[type="text"]')
						.addClass(uniqIdFromTo + '-from');
					uniqIdTo = uniqIdFromTo + '-to';
					uniqIdFrom = 0;

				} else if( params.$this.hasClass('js-datetimepicker-to') ) {
					if(uniqIdFromTo) {
						params.$this
							.find('.js-datetimepicker-calendar')
							.attr('id', uniqIdFromTo + '-to');
						params.$this.find('input[type="text"]')
							.addClass(uniqIdFromTo + '-to');
						uniqIdFrom = uniqIdFromTo + '-from';
						uniqIdFromTo = 0;
						uniqIdTo = 0;
					}
				} else {
					params.$this
								.attr('data-id', uniqId);
					uniqIdFrom = 0;
					uniqIdTo = 0;
				}

				if (typeof params.refresh != 'undefined' && params.refresh) {
					$calendar.removeClass("hasDatepicker").html("").attr("id", "");
					daterange1.val("");
					daterange2.val("");
					form_control.val("");
				}


				if(params.$this.find('input[type="text"]').data('max-date')) {
					maxDate = params.$this.find('input[type="text"]').data('max-date');
				} else if(params.$this.data('max-date')) {
					maxDate = params.$this.data('max-date');
				} else {
					maxDate = null;
				}
				if(params.$this.find('input[type="text"]').data('min-date')) {
					minDate = params.$this.find('input[type="text"]').data('min-date');
				} else if(params.$this.data('min-date')) {
					minDate = params.$this.data('min-date');
				} else {
					minDate = null;
				}


				// [Hide calendar, if click outside the calendar]
				$(document).on('click', 'html,body', function(e) {
					var $target = $(e.target);

					if ( $target.closest('.js-datetimepicker-calendar').size() == 0 ) {
						$('.hasDatepicker', params.$this).hide();
						if($parent.hasClass('archive')) {
							$parent.removeClass('open');
						}
					}
				});

			// [Diapazon date calendars]
			if(daterange1.size() > 0 && daterange2.size() > 0) {

				createSimpleCalendar({
										'$this':params.$this,
										'$calendar':$calendar,
										'form_control':form_control,
										'daterange1':daterange1,
										'daterange2':daterange2,
										'minDate': minDate,
										'maxDate': maxDate
									});


			} else {// [One date calendars]

				// [Calendar one date for select year/month if exist class js-datetimepicker-born]

				createSimpleCalendar({
										'$this':params.$this,
										'$calendar':$calendar,
										'form_control':form_control,
										'uniqIdFromTo': uniqIdTo || uniqIdFrom || 0,
										'minDate': minDate,
										'maxDate': maxDate
									});

			}
			// [Hide calendars after parsing]
			$('.hasDatepicker').hide();
		}

		// [Open calendar on click into input or calendar icon]
		$(document).on("click focus", ".js-datetimepicker-input, .js-datetimepicker-icon", function(e) {
			e.preventDefault();
				var $this = $(this).closest(".js-datetimepicker"),
					$calendar = $(".js-datetimepicker-calendar", $this),
					dataId = $this.attr("data-id"),
					$elms = $(".js-datetimepicker[data-id='" + dataId + "']"),
					index = $elms.index($this);
				$('.hasDatepicker').hide();
				$('.hasDatepicker', $this).show();
				// Castomize select in calendar
				if(index > 0) {
					// Refresh after cloning
					calendarsCycle({'$this': $this, 'refresh': 1});
				}
				setTimeout(function() {
					timePickerCustomSelect($calendar);
				}, 5);
			return false;
		});

		// [Calendar close, if close cross exist]
		$(document).on('mousedown', '.close-datepicker', function(e) {
			e.preventDefault();
				$(this).closest('.hasDatepicker').removeClass('open').hide();
				$(this).closest('.archive').removeClass('open');
			return false;
		});

		// [Parse static calendars]
		$parent.each(function(index) {
			var $this = $(this);
			calendarsCycle({
							'$this': $this,
							'$parent': $parent
						});
		});

		// [Create calendars on open modal window]
		$(".modal").on('shown.bs.modal loaded.bs.modal', function (e) {
			tabInputs();
			var $parent = $(".js-datetimepicker", $(this));
			$parent.each(function(index) {
				var $this = $(this);
				calendarsCycle({
					'$this': $this,
					'$parent': $parent
				});
			});
		});


		/**
		* Tab inputs
		*/
		function tabInputs(){
			$('.js-tab-checkbox').on('change', function(){
				$('.js-tab-checkbox').each(function(){
					var $input = $(this),
						$parent = $input.parent('label');
					if( $input.prop('checked') == true ) {
						$parent.addClass('checked');
					}else{
						$parent.removeClass('checked');
					}
				});

			});
			$('.js-checkbox-content-control').each(function(){
				var $that = $(this);
				setInterval( function() {
					if( $that.prop('checked') == true ) {
						$('.'+$that.data('content')+'').removeClass('hidden');
					}else{
						$('.'+$that.data('content')+'').addClass('hidden');
					}
				},10);
			});
			$('.js-tab-checkbox').trigger('change');
		}

	}());

	// [Cloned calendar sample]
	$(document).on('click', '.js-datetimepicker-clone', function(e) {
		e.preventDefault();
		var $this = $(this),
			$calendar = $this.prev('.js-datetimepicker:eq(0)');
		$calendar.clone(true,true).insertAfter($calendar);
	});

	// [Collapsed block in form]
	$(document).on('ifChecked ifUnchecked', '.js-icheck', function(e) {
		$(this).closest('a').trigger('click');
	});


	$('form.js-form-validate,form').each(function() {
		var $this = $(this),
			$inputs = $("input.required,textarea.required,select.required"),
			$btn = $(".btn[type='submit'],button[type='submit']",$this),
			$comment = $(".comment-error",$this);
		$this.validate({
			errorPlacement: function(error,element) {
				return true;
			}
		});
		$this.submit(function(e) {
			$('.datetimepicker,.select-wrapper-full').has('input.error,select.error').addClass('error').end().has('input.valid,select.valid').removeClass('error');
			if($(this).valid()) {
				$btn
					.removeClass('button_disabled')
					.removeAttr('disabled');
				$comment.hide();
				return true;
			} else {
				e.preventDefault();
				$btn
					.addClass('button_disabled')
					.attr('disabled', 'disabled');
				$comment.show();
				return false;
			}
		});
		$(document).on('keyup change select focusout', "input.required,textarea.required,select.required", function(e) {
			setTimeout(function() {
				$('.datetimepicker,.select-wrapper-full').not(':has(input.error,select.error)').removeClass('error');
			}, 5);

			if($this.valid()) {
				$btn
					.removeClass('button_disabled')
					.removeAttr('disabled');

				$comment.hide();
			}
		});

	});


	// [Add review]
	(function() {

		$(document).on('click', '.js-add-review', function(e) {
			e.preventDefault();

			$buttonBox = $(this).closest('.add-review-form');
			$form = $buttonBox.next('.review-form');

			$buttonBox.slideUp();
			$form.slideDown();
		});

		var readmore = $('.readmore'),
			showmore = readmore.prev('.show-more');

		showmore.slideUp(0);

		readmore.click(function(e) {

			var $this = $(this),
				$this_text = $this.text();
				$this_data = $this.data('title'),
				showmore = $(this).prev('.show-more'),
				shadow = showmore.prev('.shadow');

				shadow.toggle();

			if(showmore.size()) {
				e.preventDefault();
				showmore.slideToggle(200);

				if($this.hasClass('hide-after-click')){
					$this.fadeOut(function(){
						$this.remove();
					});
				}else{
					$this.data('title',$this_text).text($this_data);
				}
			}
		});

	}());
	// [Add review]


	// [Checkform]
	(function() {

		$('.subscribe-form').submit(function(e) {
			if($(this).valid()) {
				$(this).removeClass('form-error');
				return true;
			} else {
				e.preventDefault();
				$(this).addClass('form-error');
				return false;
			}
		});

	}());
	// [/Checkform]

	/**
	 * Kids Age and Custom scrollbar
	 */
	(function() {
		$(".bootstrap-select div.dropdown-menu:eq(0)").css({
			'height': 150
		}).mCustomScrollbar();

		$(".custom-scrollbar").each(function() {
			$(this).mCustomScrollbar({
				scrollButtons: {
					enable: false,
				},
				advanced: {
					updateOnContentResize: true,
					updateOnBrowserResize: true
				}
			});
		});
	}());


	/**
	 * init nav fixed
	 */

	(function() {

		var $navFixed = $('#nav-fixed');

		// If person closes navigation when it doen't disturb him,
		// we switch to manual mode. In manual mode we don't
		// try show navigation automatically on wide screen.
		var manualMode = false;
		var manualTimer;
		var lastWindowWidth = $(window).width();
		var breakpoint = 1500;

		$(document).on('click', '.js-nav-fixed-ctrl', toggleNav);
		$(window).resize(showNavOnWideScreen);

		function toggleNav(e) {
			e.preventDefault();

			if (isNavVisible()) {
				hideNav();
			} else {
				showNav();
			}
		}

		function isNavVisible() {
			var marginLeft = parseInt($navFixed.css('margin-left'));
			return (marginLeft == 0) ? true : false;
		}

		function hideNav() {
			$navFixed
				.removeClass('fixed-pane_show')
				.addClass('fixed-pane_hide');

			if (manualMode) return;

			if ($(window).width() >= breakpoint) {
				enableManual();
			}
		}

		function showNav() {
			$navFixed
				.removeClass('fixed-pane_hide')
				.addClass('fixed-pane_show');

			if (manualMode) return;

			preventManual();
		}

		// Set default state, if:
		// - we resize across breakpoint
		// - nav is closed when resize
		function showNavOnWideScreen() {
			if (manualMode) return;

			var windowWidth = $(window).width();

			if (!isNavVisible() && lastWindowWidth < breakpoint
					 && windowWidth >= breakpoint) {
				setDefault();
			}

			lastWindowWidth = windowWidth;
		}

		function setDefault() {
			$navFixed
				.removeClass('fixed-pane_show fixed-pane_hide');
		}

		function enableManual() {
			manualTimer = setTimeout(function() {
				manualMode = true;
			}, 2000);
		}

		function preventManual() {
			clearTimeout(manualTimer);
		}

	}());

	/**
	 * init autosize
	 */

	(function() {
		autosize($('textarea'));
	}());

	/**
	 * init select2
	 */

	(function() {

		$('.js-select-person').each(function() {
			var url = $(this).data('url'),
				$this = $(this);

			$(this).select2({
				closeOnSelect: false,
				ajax: {
					delay: 250,
					cache: false,
					url: url,
					dataType: 'json',
					processResults: function (data) {
						return {
							results: data.items
						};
					}
				},
				templateResult: function(person) {
					if (!person.id ) { return person.text; }

					if($this.hasClass('js-select-person-noavatar')) {
						var $person = $(
							'<div class="select2__person">' +
								'<div class="select2__person__name_noimage">' +
									person.text +
								'</div>' +
							'</div>'
						);
					} else {
						var $person = $(
							'<div class="select2__person">' +
								'<div class="select2__person__image" style="background-image:url(' +
									person.imageSrc +
								');">' +
								'</div>' +
								'<div class="select2__person__name">' +
									person.text +
								'</div>' +
							'</div>'
						);
					}

					return $person;
				}
			});
		});

		$('.js-select:not([multiple])').select2({
			minimumResultsForSearch: Infinity,
			allowClear: true
		});
		$('.js-select[multiple]').select2({
			minimumResultsForSearch: Infinity,
			allowClear: false
		});

	}());


	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});

	/**
	* Forms collapsed block
	*/
	(function(){

		var $collapsedHref = $('.js-form__collapsed-href'),
			$collapsedBlock = $('.js-form__collapsed-block');
		$collapsedBlock.slideUp(0);

		$collapsedHref.on('click', function(e) {
			e.preventDefault();
			var $this = $(this),
				index = $collapsedHref.index($this);
			$collapsedBlock.eq(index)
				.slideToggle(400)
				.toggleClass('open');
			$this
				.toggleClass('active');
			return false;
		});

	}());


	/**
	* Media fold/unfold content
	*/

	$('.media__unfold-heading').each(function(){

		var $button = $(this),
			$content = $button.next('.media__unfold-content');

		$button.on('click', function(){
			$content.toggleClass('open');
			$button.toggleClass('open');
		});

	});


	/**
	* Pseudo links
	*/

	$('.pseudo-link').on('click', function(event){
		event.preventDefault();
	});



	/**
	* Social Likes __ counter wrapper
	*/

	$('.social-likes').on('ready.social-likes', function() {
		$('.social-likes__counter').each(function(){
			var text = $(this).text();
			$(this).html('<span class="social-likes__counter__text">'+text+'</span>');
		});
	});

	/**
	* Dynamic accordion block
	*/
	(function(){

		var $accordions_list = $('accordion').wrapAll('<div class="accordions-list"></div>'),
			$parents_accordion = $('.accordions-list');
		$parents_accordion.each(function(index) {
			var $this = $(this),
				$accordions = $this.find('accordion'),
				$parent_accordion = $('<div/>').attr('id', 'accordion-' + index)
			$this.html('');
			$accordions.each(function(ind) {
				var $accordion = $(this),
					title = $accordion.attr('title'),
					content = $accordion.html(),
					open = ($accordion.attr('open'))? true : false,
					$accordion__item = $('<article/>')
													.addClass('accordion__item panel'),

					$h2 = $('<h2/>')
									.addClass('reset-font margin-bottom-0'),

					$a = $('<a/>')
								.attr({
									'href': '#accordion-' + index + '-' + ind,
									'data-toggle': 'collapse',
									'data-parent': '#accordion-' + index
								})
								.addClass( (open)? 'accordion__ctrl' : 'accordion__ctrl collapsed' )
								.text( title ),

					$collapse = $('<div/>')
										.addClass( (open)? 'collapse in' : 'collapse' )
										.attr('id', 'accordion-' + index + '-' + ind),

					$accordion__body = $('<div/>')
											.addClass('accordion__body'),

					$article = $('<div/>')
										.addClass('article')
										.html( content );

					$h2.append( $a );
					$accordion__body.append( $article );
					$collapse.append( $accordion__body );
					$accordion__item.append( $h2 ).append( $collapse );
					$parent_accordion.append($accordion__item);
			});
			$this.append($parent_accordion);
		});

	}());



	// torusit info list
	(function(){

		$('.personal-tourist-info__servise-list-title').each(function(){

			var $button = $(this),
				$content = $button.next('.personal-tourist-info__servise-list');

			$button.on('click', function(){
				$button.toggleClass('active');
				$content.toggleClass('active');
			});

		});

	}());


	(function(){

		$('.page__likes-fixed').on('ready.social-likes', function() {
			$(this).find('.social-likes__counter__text').each(function(){
				if($(this).text()==''){
					$(this).text('0')
				}
			});
		});

	}());


	(function(){

		$('.servise').each(function(){

			var $item = $(this),
				$button = $item.find('.servise__button'),
				$placeholder = $item.find('.servise-placeholder'),
				$content = $item.find('.servise-content'),
				$buttonTpl = $item.find('.button-add-tpl'),
				$template = $item.find('.servise-template'),
				template = $template.html(),
				$delete = $item.find('.servise__delete');



			$button.on('ifChecked', function(event){
				$placeholder.addClass('hidden');
				$content.removeClass('hidden');
			});
			$button.on('ifUnchecked', function(event){
				$placeholder.removeClass('hidden');
				$content.addClass('hidden');
				$item.find('.servise__new').remove();
			});
			$buttonTpl.on('click', function(){
				if($item.find('.servise__new').length==0){
					$template.before('<div class="row p-b-10 servise__new servise-content hidden">'+template+'</div>');
				}
				$template.before('<div class="row p-b-10 servise__new servise-content">'+template+'</div>');
				$item.find('.servise__new').last().find('.js-datetimepicker-input').datepicker('refresh');
				$item.find('.servise__new').last().find('.js-icheck').iCheck();
				return false;
			});


		});

		$(document).on('click', '.servise__delete', function(e) {
			var $button = e.currentTarget,
				$firstItem = $button.closest('.servise-first'),
				$addedItem = $button.closest('.servise__new');
			if($addedItem){
				$addedItem.remove();
			}else if($firstItem){
				$($($firstItem).find('.servise__button')[0]).iCheck('uncheck');
			}
			return false;
		});


	}());


	/**
	* Header Search Form
	*/
	(function(){
		$('.cities__map__pabel-button').on('click', function(){
			$(this).toggleClass('cities__map__pabel-button-hide');
			$(this).parent('div').toggleClass('cities__map__panel-hidden');

			if($(this).parent('div').hasClass('cities__map__search')){
				$(this).closest('.cities__map').toggleClass('p-l-0');
			}else if($(this).parent('div').hasClass('cities__map__offers')){
				$(this).closest('.cities__map').toggleClass('p-r-0');
			}
		});
		$('.header__search').each(function(){
			var $searchBox = $(this),
				$button = $searchBox.find('.header__search-button'),
				$formWrap = $searchBox.find('.header__search-form'),
				$searchField = $searchBox.find('.header__search-field'),
				hiddenClass = 'header__search-form-hide';
			$button.on('click', function(){
				if($formWrap.hasClass(hiddenClass)){
					$formWrap.removeClass(hiddenClass);
					$searchBox.addClass('open');
					$searchField.focus();
				}else{
					if($searchField.val().trim()==''){
						$searchBox.removeClass('open');
						$formWrap.addClass(hiddenClass);
					}else{
						$formWrap.submit();
					}
				}
			});
			// $searchBox.closest('header.header').on('mouseleave', function(){
			// 	$formWrap.addClass(hiddenClass);
			// });
		});

	}());


	/**
	* Review form
	*/
	(function(){
		$('.panel-review-form form').on('submit', function(){
			var valid = true,
				$form = $(this),
				action = $form.attr('action');
			$(this).find('.form-control').each(function(){
				if($(this).val().trim()==''){
					$(this).addClass('error');
					vaild = false;
				}
			});
			if(valid==true){
				$.ajax({
					url: action,
					data: $form.serializeArray(),
					method: 'post',
					success: function (data) {
						if (data.success) {
							$form[0].reset();
							$form.html(' ').append('.form-message.success').html(data.html);
						}else{
							// $form.find('.form-message').remove();
							$form.find('.form-message').html(data.html);
						}
					},
					error: function () {
						console.log('error');
					}
				 });
			}
			return false;
		});
		$('.panel-review-form form .form-control').on('keyup', function(){
			$(this).next('.form-group-error').remove();
		});
	}());


	/**
	* Cities Map
	*/
	(function(){

		$('.cities__map__map').each(function(){

			if(parseInt($('.fixed-pane').css('margin-left'))>-100){
				$('.open-leftmenu').eq(0).click();
			}
			$('.cities__map__offers-list').mCustomScrollbar();

			var $map = 	$(this),
				mapId = $map.attr('id'),
				api = $map.data('api'),
				data;
			$.ajax({
				url: api + '?=' + (new Date).getTime(),
				dataType: 'json',
				cache: false,
				success: function (data) {
					initMap(data);
					initListResults(data);
					metroSelect(data);
				},
				error: function () {
					console.log('error');
				}
			});
			$('.cities__map__search .form').on('change submit', function(){
				var handlerUrl = ''+api+'?'+$(this).serialize()+'&q='+$('.cities__search-field').val()+'';
				$.ajax({
					url: handlerUrl,
					dataType: 'json',
					cache: false,
					success: function (data) {
						initMap(data);
						initListResults(data);
						metroSelect(data);
					},
					error: function () {
						console.log('error');
					}
				});
				return false;
			});
			$('.cities__map__search__reset').on('click', function(){
				$('.cities__search')[0].reset();
				$('.cities__map__search .form')[0].reset();
				$('.cities__map__search .form').trigger('submit');
			});
			$('.cities__search').on('submit', function(){
				if(!$(this).find('.cities__search-field').val().trim()==''){
					$('.cities__map__search .form').trigger('submit');
				}
				return false;
			});
			function metroSelect(data){
				if(data.metro.length>0){
					var metroList = [];
					$('#select-metro').html(' ');
					$.each(data.metro,function(){
						var metroValue = $(this)[0].id,
							metroName = $(this)[0].value;
						$('#select-metro').append('<option value="'+metroValue+'">'+metroName+'</option>')
					});
				}
			}
			function initListResults(data){
				$('.cities__map__offers-list').find('.mCSB_container').html(' ');
				var results = data.organizations,
					orgLeng = data.organizations.length;
				$('.cities__map__offers-titile__count').text(orgLeng);
				$.each(results, function(){
					var imgTpl = '';
					if(!$(this)[0].image==''){
						imgTpl = '<img src="'+$(this)[0].image+'" alt="" class="cities__map__offers-item__img">'
					}
					$('.cities__map__offers-list').find('.mCSB_container').append(
						'<a class="cities__map__offers-item" href="'+$(this)[0].link+'" data-id="'+$(this)[0].id+'">'+
							imgTpl+
							'<span class="cities__map__offers-item__text">'+
							    $(this)[0].title+
							'</span>'+
						'</a>'
					);
				});
				initOrgFilter();
			}
			function initOrgFilter(){
				$('.cities__map__offers-item').on('click', function(){
					var handlerUrl = ''+api+'?'+$('.cities__map__search form').serialize()+'&q='+$('.cities__search-field').val()+'&org_id='+$(this).data('id')+'';
					$.ajax({
						url: handlerUrl,
						dataType: 'json',
						cache: false,
						success: function (data) {
							initMap(data);
							initListResults(data);
						},
						error: function () {
							console.log('error');
						}
					});
					return false;
				});
			}
			function initMap(data){
				$('.cities__map__map').html(' ');
				var mapZoom = data.map[0].zoom,
					mapCenterX = data.map[0].centerx,
					mapCenterY = data.map[0].centery,
					points = data.points;

				ymaps.ready(function () {
					var map = new ymaps.Map(mapId, {
							center: [mapCenterX, mapCenterY],
							zoom: mapZoom,
							controls: [],
							checkZoomRange: true
						});
					map.controls
						.add('zoomControl', {
							size: 'small'
						})
						/*
						.add('typeSelector', {
							position: 'left'
						})
						*/
						.add('fullscreenControl', {
							position: 'left'
						});
					map.behaviors.disable("scrollZoom");
					$('.cities__map__pabel-button').on('click', function(){
						setTimeout(function(){
							map.container.fitToViewport();
						},300);
					});
					var clusterer = new ymaps.Clusterer({
							clusterIcons: [{
							    href: 'images/cities/map-group.png',
							    size: [72, 72],
							    offset: [-27,-27]
							}],
							preset: 'islands#darkorangeClusterIcons'
						});
					var pointIconML = 17.5,
						pointIconMT = 20.5,
						iterate = 0;
					$.each(points, function(){
						iterate += 1;
						var pointData = $(this)[0],
							pointId = pointData.id,
							pointType = pointData.type,
							pointIconW = 35,
							pointIconH = 41,

							pointX = pointData.coordx,
							pointY = pointData.coordy,
							desc = pointData.desc,
							text = pointData.text,

							headertext = pointData.headertext,
							rating = pointData.rating,
							title = pointData.title,
							image = pointData.image,

							address = pointData.address ,
							metro = pointData.metro ,
							phone = pointData.phone ,
							web = pointData.web ,
							events = pointData.events,
							eventsTpl = '',

							reviewbutton = pointData.reviewbutton,
							ratingbutton = pointData.ratingbutton,
							errorbutton = pointData.errorbutton,

							title = pointData.title;

							// Форма "Оставить отзыв"
							var fr = pointData.formreview;
							var frTpl = '';
							if( typeof fr !== 'undefined') {

								frAction  = fr.action;

								frTpl += '<div id="review" class="tab-pane" role="tabpanel">'+
										 '<form id="formreview-' + pointId + '" class="formreview formreview-' + pointId + '" action="' + frAction + '" method="POST">'+
										 '<input type="hidden" name="id" value="' + pointId + '" />';
								frTpl += '<div class="row p-l-10 p-r-10">'+ // [start row]
											'<div class="col-xs-24 p-l-0 p-r-0 cp-subtitle__text"><p>Оставьте отзыв</p></div>'+
											'<div class="row m-b-20">';

									frTpl += 	'<div class="col-xs-12">'+
												'<input class="form-control w-100 required" name="name" type="text" placeholder="Имя" />'+
												'</div>';

									frTpl += 	'<div class="col-xs-12">'+
												'<input class="form-control w-100 required email" name="email" type="email" placeholder="Эл. почта" />'+
												'</div>';

									frTpl += '</div><div class="row m-b-20">';

									frTpl += 	'<div class="col-xs-24">'+
												'<textarea class="form-control required" name="review" placeholder="Ваш отзыв"></textarea>'+
												'</div>';

								frTpl += 	'</div>'; // [/end row]
								frTpl += 	'<div class="row m-b-10">'+
											'<div class="col-xs-9">';
								frTpl += 	'<button type="submit" class="button button_disabled button_blue-dark button_blue-dark_size-3"><span class="button__text" disabled>Опубликовать</span></button>';
								frTpl += 	'</div>'+
											'<div class="col-xs-15">'+
												'<div class="cp-comment__text">Необходимо заполнить поля: имя, эл. почта ваш отзыв</div>'+
											'</div>'+
											'</div>';
								frTpl +=
										'</div>'+
										'<span class="result"></span>'+
										'</form>'+
										'</div>';

							}

							// Форма "Оценить"
							var frt = pointData.formreview;
							var frtTpl = '';
							if( typeof frt !== 'undefined') {

								frtAction  = frt.action;

								frtTpl += '<div id="rating" class="tab-pane" role="tabpanel">'+
										 '<form id="formrating-' + pointId + '" class="formrating formrating-' + pointId + '" action="' + frtAction + '" method="POST">'+
										 '<input type="hidden" name="id" value="' + pointId + '" />'+
										 '<div class="row p-l-10 p-r-10">'+
											'<div class="col-xs-24 p-l-0 p-r-0 cp-subtitle__text"><p>Поставьте оценку от 1 до 5 в каждом параметре:</p></div>'+
										 '</div>';
								frtTpl += '<div class="row">';

									// [Проставляем рейтинги]
									frtTpl += '<div class="col-xs-6">';
										frtTpl += '<div class="js-raty cp-raty" data-score-name="purity"></div>';
										frtTpl += '<div class="cp-subtitle__text cp-subtitle__text--label">Чистота</div>';
									frtTpl += '</div>';
									frtTpl += '<div class="col-xs-6">';
										frtTpl += '<div class="js-raty cp-raty" data-score-name="comfort"></div>';
										frtTpl += '<div class="cp-subtitle__text cp-subtitle__text--label">Комфорт</div>';
									frtTpl += '</div>';
									frtTpl += '<div class="col-xs-6">';
										frtTpl += '<div class="js-raty cp-raty" data-score-name="relationship_to_child"></div>';
										frtTpl += '<div class="cp-subtitle__text cp-subtitle__text--label">Отношение к ребенку</div>';
									frtTpl += '</div>';
									frtTpl += '<div class="col-xs-6">';
										frtTpl += '<div class="js-raty cp-raty" data-score-name="program"></div>';
										frtTpl += '<div class="cp-subtitle__text cp-subtitle__text--label">Программа</div>';
									frtTpl += '</div>';
									// [/Проставляем рейтинги]

								frtTpl += '</div>';
								frtTpl += 	'<div class="row m-b-10">'+
											'<div class="col-xs-9">';
								frtTpl += 	'<button type="submit" class="button button_disabled button_blue-dark button_blue-dark_size-3"><span class="button__text" disabled>Опубликовать</span></button>';
								frtTpl += 	'</div>'+
											'<div class="col-xs-15">'+
												'<div class="cp-comment__text p-t-10">Необходимо поставить все оценки</div>'+
											'</div>'+
											'</div>';
								frtTpl +=
										'</div>'+
										'<span class="result"></span>'+
										'</form>'+
										'</div>';

							}

							// Форма "Сообщить об ошибке"
							var fe = pointData.formerror;
							var feTpl = '';
							if( typeof fe !== 'undefined') {

								feAction  = fe.action;

								feTpl = '<div id="error" class="tab-pane" role="tabpanel">'+
										'<form id="formerror-' + pointId + '" class="formerror formerror-' + pointId + '" action="' + feAction + '" method="POST">'+
										'<input type="hidden" name="id" value="' + pointId + '" />'+
										'<div class="row p-l-10 p-r-10">'+ // [start row]
											'<div class="col-xs-24 p-l-0 p-r-0 cp-subtitle__text"><p>Сообщите нам об ошибке.</p></div>'+
											'<div class="row m-b-20">';

									feTpl += 	'<div class="col-xs-12"><input class="form-control w-100 required" name="name" type="text" placeholder="Имя" /></div>';

									feTpl += 	'<div class="col-xs-12"><input class="form-control w-100 required email" name="email" type="email" placeholder="Эл. почта" /></div>';

									feTpl += '</div><div class="row m-b-20">';

									feTpl += 	'<div class="col-xs-24">'+
												'<textarea class="form-control required" name="review" placeholder="Ваш отзыв"></textarea>'+
												'</div>';

								feTpl += 	'</div>'; // [/end row]
								feTpl += 	'<div class="m-b-10"><button type="submit" class="button button_disabled button_blue-dark button_blue-dark_size-3"><span class="button__text" disabled="">Отправить</span></button></div>';
								feTpl +=
										'</div>'+
										'<span class="result"></span>'+
										'</form>'+
										'</div>';

							}

						// if(pointType=='default'){
						// 	pointIconW = 35;
						// 	pointIconH = 41;
						// 	pointIconML = 17.5;
						// 	pointIconMT = 20.5;
						// }

						$.each(events, function(){
							eventsTpl = eventsTpl +
								'<div class="cp-table__row">'+
									'<div class="cp-table__col">'+$(this)[0].name+'</div>'+
									'<div class="cp-table__col">'+$(this)[0].age+'</div>'+
									'<div class="cp-table__col">'+$(this)[0].schedule+'</div>'+
									'<div class="cp-table__col">'+
										$(this)[0].availabilityRu+
										'<span class="cp-table__link">'+$(this)[0].link+'</span>'+
										'</div>'+
								'</div>';
						});
						var marker = new ymaps.Placemark([pointX,pointY],{
								hintContent: desc,
								balloonContentBody: [
									'<div class="cities__map__card">'+
										'<div class="cp-header">'+
											'<div class="cp-header__text">'+headertext+'</div>'+
											'<div class="cp-header__rating"><img src="images/cities/map-rating-'+rating+'.png"></div>'+
										'</div>'+
										'<div class="cp-title">'+
											'<div class="cp-title__icon"><img src="'+image+'"></div>'+
											'<div class="cp-title__text">'+title+'</div>'+
										'</div>'+

										// [Область переключения табов/контента]

											'<div class="tab-content">'+

												'<div id="info" class="tab-pane active" role="tabpanel">'+
													'<div class="cp-description">'+
														'<div class="cp-description-row">'+address+'</div>'+
														'<div class="cp-description-row">'+metro+'</div>'+
														'<div class="cp-description-row">'+
															'<div class="cp-description-col">'+phone+'</div>'+
															'<div class="cp-description-col weblink">'+web+'</div>'+
														'</div>'+
													'</div>'+
													'<div class="cp-table">'+
														'<div class="cp-table__header">'+
															'<div class="cp-table__row">'+
																'<div class="cp-table__col">Направления:</div>'+
																'<div class="cp-table__col">Возраст:</div>'+
																'<div class="cp-table__col">График занятий:</div>'+
																'<div class="cp-table__col">Доступные места:</div>'+
															'</div>'+
														'</div>'+
														'<div class="cp-table__body">'+
															eventsTpl+
														'</div>'+
													'</div>'+
												'</div>'+

												frTpl+

												feTpl+

												frtTpl+

											'</div>'+

										// [/Область переключения табов/контента]

										'<ul class="cp-footer nav-tabs" role="tablist">'+
											// ["Копки" табов]
											'<li role="presentation" class="cp-footer__col active">'+(reviewbutton? '<a href="#info" aria-controls="info" role="tab" data-toggle="tab"><span class="info"></span>Информация</a>': '')+'</li>'+
											'<li role="presentation" class="cp-footer__col">'+(reviewbutton? '<a href="#rating" aria-controls="star" role="tab" data-toggle="tab"><span class="star"></span>Оценить</a>': '')+'</li>'+
											'<li role="presentation" class="cp-footer__col">'+(ratingbutton? '<a href="#review" aria-controls="review" role="tab" data-toggle="tab"><span class="review"></span>Оставить отзыв</a>': '')+'</li>'+
											'<li role="presentation" class="cp-footer__col">'+(errorbutton? '<a href="#error" aria-controls="error" role="tab" data-toggle="tab"><span class="error"></span>Сообщить об ошибке</a>': '')+'</li>'+
											// [/"Копки" табов]
										'</ul>'+
									'</div>'
            					],
            					clusterCaption: title
							},{
								iconLayout: 'default#image',
								iconImageHref: pointType,
								iconImageSize: [pointIconW, pointIconH],
								iconImageOffset: [-pointIconML, -pointIconMT]
							});
						clusterer.add(marker);
						map.geoObjects.add(clusterer);
					});

					// [Корректируем размер и позицию балуна]
					var tabHeightContent = 0,
						tabHeightCorrect = 0,
						topPosDefault = 0;
					$(document).on('click', '.cp-footer a', function(e) {
						setTimeout(function() {
							var $heightCorect = $("ymaps[class*='-balloon_layout_normal']"),
								$tabContent = $("ymaps[class*='-balloon__content'] .tab-content"),
								tabContentH = $tabContent.height(),
								$tabHeightCorrect = $("ymaps[class*='-balloon__content']>ymaps"),
								//tabHeightCorrect = $tabHeightCorrect.height(),
								$topPosCorrect = $("ymaps[class*='-balloon_layout_normal']"),
								correctHeight = (tabContentH > tabHeightContent)?  +(tabContentH - tabHeightContent) : -(tabHeightContent - tabContentH);
							$tabHeightCorrect.animate({
								'height': tabHeightCorrect + correctHeight
							}, 100);

							$topPosCorrect.animate({
								'top': topPosDefault - correctHeight
							}, 400);

							// Валидация форм
							var $validateFields = $tabContent.find('input.required,textarea.required');
							$validateFields
								.parents('form').each(function() {
									$(this).validate({
										errorPlacement: function(error,element) {
											return true;
										}
									});
								});
							$validateFields.on('keyup', function() {
								var $form = $(this).parents('form').eq(0);

								console.log($form.valid());

								if($form.valid()) {
									$form.find('button[type="submit"]')
										.removeClass('button_disabled')
										.attr("disabled", false)
										.prop("disabled", false);
								}

							});

							// Форма с рейтингом
							var $raty = $('.js-raty');

							if( $raty.size() ) {

								$('.js-raty').raty({
									space: false,
									scoreName: function() {
										return $(this).data('score-name');
									},
									iconRange: [
										{ range: 1, on: 'images/cities/raty/1-on.png', off: 'images/cities/raty/1-off.png' },
										{ range: 2, on: 'images/cities/raty/2-on.png', off: 'images/cities/raty/2-off.png' },
										{ range: 3, on: 'images/cities/raty/3-on.png', off: 'images/cities/raty/3-off.png' },
										{ range: 4, on: 'images/cities/raty/4-on.png', off: 'images/cities/raty/4-off.png' },
										{ range: 5, on: 'images/cities/raty/5-on.png', off: 'images/cities/raty/5-off.png' }
									],
									click: function(){
										setTimeout(function(){
											// проверка заполнения рейтинга
											var frmVld = true;
											$('form.formrating').find('input').each(function(){
												if( $(this).val().trim() == '' ) {
													frmVld = false;
												}
											});
											if(frmVld) {
												$('form.formrating').find('.button').removeClass('button_disabled');
											}
										},200)
									}
								});

								var $ratyImgs = $raty.find('img');
								$ratyImgs
									.on('mouseenter', function() {
										var $this = $(this);
										$ratyImgs.removeClass('hover');
										$this.prevAll('img').addClass('hover');
									})
									.on('mouseleave', function() {
										$ratyImgs.removeClass('hover');
									});
							}
							setTimeout(function(){
								// отправка формы отзыва

								$('form.formrating, form.formreview, form.formerror').on('submit', function(){
									console.log('submit');
									var $thatForm = $(this),
										actn = $thatForm.attr('action'),
										msg = $thatForm.serialize();
									$.ajax({
										type: 'POST',
										url: actn,
										data: msg,
										success: function(data) {
											$thatForm[0].reset();
											$thatForm.find('.result').html(data);
											$('a[href="#'+$thatForm.closest('.tab-pane').attr('id')+'"]').click();
											if($thatForm.hasClass('formrating')){
												console.log('ds')
												$thatForm.find('.button').addClass('button_disabled');
											}
										},
										error:  function(xhr, str){
											$thatForm.find('.result').html('Возникла ошибка: ' + xhr.responseCode);
											$('a[href="#'+$thatForm.closest('.tab-pane').attr('id')+'"]').click();
										}
									});

									return false;
								});

							},1000);


						}, 100);
					});
					// [/Корректируем размер и позицию балуна]

					map.balloon.events.add('open', function(event){

						setTimeout(function(){
							$('.cp-table__body').mCustomScrollbar();

							// Get Default size Baloon
							tabHeightContent = $("ymaps[class*='-balloon__content'] .tab-content").height();
							tabHeightCorrect = $("ymaps[class*='-balloon__content']>ymaps").height();
							topPosDefault = parseInt($("ymaps[class*='-balloon_layout_normal']").css('top'));
						}, 400);
						checkBallonClass();

					});

					map.geoObjects.events.add('click', function (e) {

						var geoObject = e.get('target');
						var projection = map.options.get('projection');
						var position = geoObject.geometry.getCoordinates();

						var position_global_px = map.converter.pageToGlobal(projection.fromGlobalPixels(position, map.getZoom()));
						var position_local_px = map.converter.globalToPage(projection.toGlobalPixels(position,map.getZoom()));
						map.setGlobalPixelCenter([position_global_px[0] + position_local_px[0], position_global_px[1] + position_local_px[1]]);

						setTimeout(function(){
							var mapBounds = map.getBounds(),
								mapBoundsArr = [mapBounds[0], mapBounds[1]],
								mapCenter = map.getCenter();

							var $blloon = $('[class*="-balloon_layout"]'),
								blloonH = $blloon.height() + pointIconML,
								blloonW = $blloon.width(),
								offset = $blloon.parents('[class*="-map-ru"]').offset();

							var $map = $('[class*="-map-ru"]'),
								mapW = $map.width(),
								mapH = $map.height();

							// Позиция перемещения, необходимая для центрирования Балуна
							var deltaH = (blloonH - (mapH / 2) + (mapH - blloonH) / 2) / mapH, // положение при правильной центровке
								deltaW = (blloonW - (mapW / 2) - 2 * 30 + (mapW - blloonW) / 2) / mapW, // положение при правильной центровке [2 * 30 - учет отступов]
								dY = (Number(mapBounds[1][0]) - Number(mapBounds[0][0])) * deltaH,
								dX = (Number(mapBounds[1][1]) - Number(mapBounds[0][1])) * deltaW, // 0.255
								dYCenter = Number(position[0]) + dY,
								dXCenter = Number(position[1]) + dX;

								if($blloon.size()) {
									map.setCenter([dYCenter, dXCenter], map.getZoom());
								}

						}, 400);

					}, this);

					function checkBallonClass(){
						if ($('.ymaps-2-1-34-balloon__content').find('.ymaps-2-1-34-b-cluster-tabs__menu').length > 0){
							$('.ymaps-2-1-34-balloon__content').addClass('big');
						}else{
							$('.ymaps-2-1-34-balloon__content').removeClass('big');
						}
					}
				});
			}
		});

	}());


	/**
	 * Cities Tabs
	 */
	(function() {
		$('.cities-reviews__tab').each(function(i){
			$(this).on('click', function(){
				$('.cities-reviews__tab').removeClass('cities-reviews__tab--active')
				$('.cities-reviews__tab-content').removeClass('active');
				$('.cities-reviews__tab').eq(i).addClass('cities-reviews__tab--active')
				$('.cities-reviews__tab-content').eq(i).addClass('active');
				return false;
			});
		});
		$('.cities-faq__item').on('click', function(){
			if($(this).next('.cities-faq__cont').hasClass('open')){
				$(this).next('.cities-faq__cont').removeClass('open');
			}else{
				$('.cities-faq__cont').removeClass('open');
				$(this).next('.cities-faq__cont').addClass('open');
			}
		});
	}());


	/**
	 * Cities Tabs
	 */
	(function() {
		var $jsNavPageLink = $('.js-nav-page__link'),
			$parent = $jsNavPageLink.parents("[class^='js-']").eq(0),
			parentClass = $parent.attr('class'),
			regexp = /js-([a-z\-]*)\s+.*/g,
			result;

		if($parent.size()) {
			result = regexp.exec(parentClass);
			$jsNavPageLink.each(function() {
				var $this = $(this),
					$parent = $this.parents("[class^='js-']").eq(0);
				if(!$this.hasClass('collapsed')) $parent.addClass(result[1]);
			});

			$jsNavPageLink.on('click', function() {
				var $this = $(this),
					$parent = $(this).parents("[class^='js-']").eq(0);
				if($this.hasClass('collapsed')) {
					$parent.addClass(result[1]);
				} else {
					$parent.removeClass(result[1]);
				}
			});
		}

	}());


	/**
	 * Profile Toggle Class 'disabled'
	 */
	(function() {
		$('[data-js="checkbox-toggleclass"]').each(function(){
			var $checkbox = $(this),
				className = $checkbox.data('class'),
				$block = $('[data-block="'+$checkbox.data('target')+'"]');
			$checkbox.on('change', function(){
				if( $checkbox.prop('checked') ) {
					$block.addClass(className);
				}else{
					$block.removeClass(className);
				}
			});
		});

	}());

	/**
	* Cloned Element Select
	*/
	(function() {
		var $cloneTarget = $('.js-clone-target');
		$cloneTarget.on('change', function(e) {
			var $this = $(this),
				$target = $($this.data('target')).eq(0) || $(),
				$nextAll = $target.add($target.nextAll()),
				i = 0,
				len = +$this.find('option:selected').text(),
				nextLen = $nextAll.size();
			len = (nextLen > len)? (0, $nextAll.slice(len).remove()) : len - nextLen ;
			if($target) {
				for(; i < len; i++) $target.clone(true).insertAfter($target);
			}
		});
	}());

	/**
	* SubscriptionForm
	*/
	(function() {
		var $SubscriptionForm = $('[js-SubscriptionForm]'),
				timing = $SubscriptionForm.data('timing'),
				openClass = 'is-open',
				$SubscriptionFormButton = $SubscriptionForm.find('[js-SubscriptionForm__button]');

		openWithTimeOut = setTimeout(function(){
			$SubscriptionForm.addClass(openClass);
			$SubscriptionFormButton.addClass(openClass);
		}, timing);

		$SubscriptionFormButton.on('click', function(){
			$SubscriptionForm.toggleClass(openClass);
			$SubscriptionFormButton.toggleClass(openClass);
			clearTimeout(openWithTimeOut);
		});

		$(window).on('load', function(){
			openWithTimeOut();
		});


	}());

  // simple BIG calendar init
  $(".calendar-big" ).datepicker({
    showOtherMonths: true
  });


});
