function checkForm(form) {
    var params = {};
    $(form).find('input,select,textarea').each(function(index, elem) {
        if ((elem.type != 'checkbox' && elem.type != 'radio') || elem.checked)
            params[elem.name] = elem.value;
    });
    //console.dir(params);
    $.post(form.action, params, function(data) {

        if (data.success) {
            document.location=document.location;
        }
        else {

            var target = $(form).closest('.modal');
            $(target).find('.modal-content').html(data);
            $(target).trigger('loaded.bs.modal');
        }
        //console.dir(data);

    })
    //console.dir(params);

    return false;
}

jQuery(function($) {

    /// load more testimonials
    $('.readmore-testimonials').click(function(e){
        var _this = $(this);
       e.preventDefault();
       _this.addClass('disabled');
       $.getJSON('?page=' + _this.data('next-page') + '&type_id=' + _this.data('type-id'), '', function(data, textStatus) {
           _this.before(data.content);
           if(data.showmore === false) {
                _this.remove();
           } else {
               _this.removeClass('disabled');
               _this.data('next-page', data.nextPage);
           }
       });
    });

    $('.readmore-counselor-feedbacks').click(function(e){
        var _this = $(this);
        e.preventDefault();
        _this.addClass('disabled');
        $.getJSON('?page=' + _this.data('next-page') + '&id=' + _this.data('id'), '', function(data, textStatus) {
            _this.before(data.content);
            if(data.showmore === false) {
                _this.remove();
            } else {
                _this.removeClass('disabled');
                _this.data('next-page', data.nextPage);
            }
        });
    });

    $('.delete-tourist').on('click', function() {
        if (confirm('�� �������, ��� ������ �������?')) {
            var me = this;
            $.getJSON('?op=delete&pos=' + $(me).attr('data-id'), function(data, textStatus) {
                if (data.success) {
                    location.reload();
                    $(me).closest('tbody').hide();
                }
            });
        }
    });



    $("#reservation-form").on('submit',function(){
        $(this).addClass('form_loading');
    });

    $(document).on('submit','#add-service-form',function(){
        $(this).addClass('form_loading');
    });


    $(".service-checkbox").on('ifToggled',function(e){

        var $form =$("#add-service-form"),
            $totalPriceWrapper = $("#total_price");
        $form.ajaxSubmit({
            dataType: 'json',
            beforeSubmit: function() {
                $form.addClass('form_loading');
            },
            success: function(data) {

                if (data.error == 1) {
                    $form.removeClass('form_loading');
                }
                else {
                    $form.removeClass('form_loading');
                    $totalPriceWrapper.text(data.cert.price);

                }
            },
            error: function() {
                $form.removeClass('form_loading');
            }

        });

    });

    $(".service-checkbox").on('ifChecked',function(e){

        var $this = $(this),
            $service_id = $this.data('service-id'),
            $targetDD = $(".qty-dd-"+$service_id);

        $targetDD.removeAttr('disabled');


    });

    $(".service-checkbox").on('ifUnchecked',function(e){

        var $this = $(this),
            $service_id = $this.data('service-id'),
            $targetDD = $(".qty-dd-"+$service_id);

        $targetDD.attr('disabled','disabled');


    });


    $(".qty-of-service").on('change',function(e){

        var $this = $(this),
            $form =$("#add-service-form"),
            $totalPriceWrapper = $("#total_price");

        $form.ajaxSubmit({
            dataType: 'json',
            beforeSubmit: function() {
                $form.addClass('form_loading');
            },
            success: function(data) {

                if (data.error == 1) {
                    $form.removeClass('form_loading');
                }
                else {
                    $form.removeClass('form_loading');
                    $totalPriceWrapper.text(data.cert.price);

                }
            },
            error: function() {
                $form.removeClass('form_loading');
            }

        });
    });



    $(".add-service-trigger").on('change',function(e){

        var $this = $(this),
            $form =$("#add-service-form"),
            $totalPriceWrapper = $("#total_price");
        if($this.val()!=''){

            $form.ajaxSubmit({
                dataType: 'json',
                beforeSubmit: function() {
                    $form.addClass('form_loading');
                },
                success: function(data) {

                    if (data.error == 1) {
                        $form.removeClass('form_loading');
                    }
                    else {
                        $form.removeClass('form_loading');
                        $totalPriceWrapper.text(data.cert.price);

                    }
                },
                error: function() {
                    $form.removeClass('form_loading');
                }

            });
            
        }
    });

    $(".noMiddleNameTrigger").on('ifChecked', function (e) {
        var middleNameField = $("#middleName");
        middleNameField.attr('disabled','disabled');
    });

    $(".noMiddleNameTrigger").on('ifUnchecked', function (e) {
        var middleNameField = $("#middleName");
        middleNameField.removeAttr('disabled');
    });

    $(".tourist-trigger").on('ifToggled',function(e){

        var $this = $(this),
            $locationId = $this.data('room-type'),
            $target = $('#tourists'+$locationId);
        changeParam();

        $target.show();

    });

    $(".js-datetimepicker-calendar").not("input[class^='js-datetimepicker-date-']+.js-datetimepicker-calendar").each(function(index) {
        var prevHandler = $(this).datepicker('option','onSelect');

        $(this).datepicker( 'option' , 'onSelect', function(date,instance) {
            prevHandler(date,instance);
            changeParam();
        });

    });
    function changeParam(){
        var $this = $(this),
            $form =$("#change-tour-param"),
            $totalPriceWrapper = $("#total_price");
        $form.ajaxSubmit({
            dataType: 'json',
            beforeSubmit: function() {
                $form.addClass('form_loading');
            },
            success: function(data) {

                if (data.error == 1) {
                    $form.removeClass('form_loading');
                }
                else {
                    $form.removeClass('form_loading');
                    $totalPriceWrapper.text(data.price);

                }
            },
            error: function() {
                $form.removeClass('form_loading');
            }

        });
    }


    setTimeout(function(){

        var $form = $("#add-service-form");
        $form.ajaxSubmit({
            beforeSubmit: function() {
                $form.addClass('form_loading');
            },
            complete:function(){
                $form.removeClass('form_loading');
            }
        });
    },500);
});