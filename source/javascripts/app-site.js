/*
 * Minimal classList shim for IE 9
 * By Devon Govett
 * MIT LICENSE
 */
 
 
if (!("classList" in document.documentElement) && Object.defineProperty && typeof HTMLElement !== 'undefined') {
    Object.defineProperty(HTMLElement.prototype, 'classList', {
        get: function() {
            var self = this;
            function update(fn) {
                return function(value) {
                    var classes = self.className.split(/\s+/),
                        index = classes.indexOf(value);
 
                    fn(classes, index, value);
                    self.className = classes.join(" ");
                }
            }
 
            var ret = {                    
                add: update(function(classes, index, value) {
                    ~index || classes.push(value);
                }),
 
                remove: update(function(classes, index) {
                    ~index && classes.splice(index, 1);
                }),
 
                toggle: update(function(classes, index, value) {
                    ~index ? classes.splice(index, 1) : classes.push(value);
                }),
 
                contains: function(value) {
                    return !!~self.className.split(/\s+/).indexOf(value);
                },
 
                item: function(i) {
                    return self.className.split(/\s+/)[i] || null;
                }
            };
            
            Object.defineProperty(ret, 'length', {
                get: function() {
                    return self.className.split(/\s+/).length;
                }
            });
 
            return ret;
        }
    });
}

$(function () {

	if(document.getElementById('js-audio')) {
		/* [Drawing buttons] */
		(function() {
			
			CanvasRenderingContext2D.prototype.line = function(x1, y1, x2, y2) {
				this.lineCap = 'round';
				this.beginPath();
				this.moveTo(x1, y1);
				this.lineTo(x2, y2);
				this.closePath();
				this.stroke();
			}
			CanvasRenderingContext2D.prototype.circle = function(x, y, r, fill_opt) {
				this.beginPath();
				this.arc(x, y, r, 0, Math.PI * 2, true);
				this.closePath();
				if (fill_opt) {
					this.fillStyle = 'rgba(0,170,187,1)';
					this.fill();
					this.stroke();
				} else {
					this.stroke();
				}
			}
			CanvasRenderingContext2D.prototype.rectangle = function(x, y, w, h, fill_opt) {
				this.beginPath();
				this.rect(x, y, w, h);
				this.closePath();
				if (fill_opt) {
					this.fillStyle = 'rgba(0,170,187,1)';
					this.fill();
				} else {
					this.stroke();
				}
			}
			CanvasRenderingContext2D.prototype.triangle = function(p1, p2, p3, fill_opt) {
				// Stroked triangle.
				this.beginPath();
				this.moveTo(p1.x, p1.y);
				this.lineTo(p2.x, p2.y);
				this.lineTo(p3.x, p3.y);
				this.closePath();
				if (fill_opt) {
					this.fillStyle = 'rgba(0,170,187,1)';
					this.fill();
				} else {
					this.stroke();
				}
			}
			CanvasRenderingContext2D.prototype.clear = function() {
				this.clearRect(0, 0, this.canvas.clientWidth, this.canvas.clientHeight);
			}

			var canvas = document.getElementById('js-audio__playbutton');
			var ctx = canvas.getContext('2d');
			ctx.strokeStyle = 'rgba(0,170,187,1)';
			ctx.lineWidth = 3;

			var R = canvas.width / 2;
			var STROKE_AND_FILL = false;

			canvas.addEventListener('mouseover', function(e) {
				if (this.classList.contains('playing')) {
					drawPauseButton(STROKE_AND_FILL);
				} else {
					drawPlayButton(STROKE_AND_FILL);
				}
				ctx.save();
				ctx.lineWidth += 0;
				ctx.circle(R, R, R - ctx.lineWidth + 1);
				ctx.restore();
			}, true);

			canvas.addEventListener('mouseout', function(e) {
				if (this.classList.contains('playing')) {
					drawPauseButton(STROKE_AND_FILL);
				} else {
					drawPlayButton(STROKE_AND_FILL);
				}
			}, true);

			canvas.addEventListener('click', function(e) {
				this.classList.toggle('playing');
				if (this.classList.contains('playing')) {
					drawPauseButton(STROKE_AND_FILL);
					audio.play();
				} else {
					drawPlayButton(STROKE_AND_FILL);
					audio.pause();
				}
			}, true);

			function drawPlayButton(opt_fill) {
				ctx.clear();
				ctx.circle(R, R, R - ctx.lineWidth + 1, opt_fill);
				ctx.triangle({x: R*0.8, y: R*0.56}, {x: R*1.45, y: R}, {x: R*0.8, y: R*1.45}, true);
			}

			function drawPauseButton(opt_fill) {
				ctx.clear();
				ctx.circle(R, R, R - ctx.lineWidth + 1, opt_fill);
				ctx.save();
				ctx.lineWidth += 0;
				ctx.line(R*0.8, R/2, R*0.8, R*1.5);
				ctx.line(R+(R/5), R/2, R+(R/5), R*1.5);
				ctx.restore();
			}
			
			drawPlayButton(STROKE_AND_FILL);

			window.playButton = canvas;
		
		})();
		/* [/Drawing buttons] */
		
		/* [Drawing equalizer] */
		(function() {
			var canvas11 = document.getElementById('fft11'),
				ctx11 = canvas11.getContext('2d');
				// canvas11.width = document.body.clientWidth / 1.4,
				canvas12 = document.getElementById('fft12'),
				ctx12 = canvas12.getContext('2d');
				// canvas11.width = document.body.clientWidth / 1.4
				;
			var audioDuration = 0;
			var canvas21 = document.getElementById('fft21'),
				ctx21 = canvas21.getContext('2d');
				// canvas21.width = canvas11.width,
				canvas22 = document.getElementById('fft22'),
				ctx22 = canvas22.getContext('2d');
				// canvas22.width = canvas11.width
				;
			var audioPane = document.getElementById("js-audio__pane");
			var audioPaneDefault = document.getElementById("js-audio__pane__default");
			var colorLine = document.getElementById("js-audio__pane__colorline");
			var jsAudio = document.getElementById("js-audio");
			var context,
				analyser;
			var currenTimeNode = document.querySelector('#audio__pane__current-time');
			
			var CANVAS_HEIGHT = canvas11.height;
			var CANVAS_WIDTH = canvas11.width;

			window.audio = new Audio();
			audio.src = jsAudio.getAttribute('data-audio');
			audio.controls = true;
			// audio.autoplay = true;
			audio.loop = true;
			
			// [?????????? ???????? ???????]
			audio.addEventListener('timeupdate', function(e) {
				var currTime = audio.currentTime,
					currMin = parseInt(currTime / 60),
					currSec = parseInt(currTime % 60);
				currSec = (currSec < 10)? '0' + currSec: currSec;	
				currenTimeNode.textContent = currMin + ':' + currSec;

				if(audioDuration > 0) {
					audioPane.style.width = Math.floor((currTime / audioDuration) * CANVAS_WIDTH) + 'px';
					colorLine.style.width = audioPane.style.width;
				} else {
					audioDuration = audio.duration;
				}
			}, false);

			jsAudio.appendChild(audio);

			// Check for non Web Audio API browsers.
			if (!window.AudioContext) {
				//alert("Web Audio isn't available in your browser. But...you can still play the HTML5 audio :)");
				audioPane.className += ' audio__pane__eq_show';
				audioPaneDefault.className += ' audio__pane__eq_show';
				return;
			}

			function rafCallback(time) {
				window.requestAnimationFrame(rafCallback, canvas11);

				var freqByteData = new Uint8Array(analyser.frequencyBinCount);
				analyser.getByteFrequencyData(freqByteData); //analyser.getByteTimeDomainData(freqByteData);

				var SPACER_WIDTH = 1;
				var BAR_WIDTH = 1;
				var OFFSET = (freqByteData.length > CANVAS_WIDTH)? freqByteData.length - CANVAS_WIDTH * 1.5: 100;
				var CUTOFF = 5;

				var numBars = Math.round(CANVAS_WIDTH / SPACER_WIDTH);

				ctx11.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx11.fillStyle = '#F6D565';
				ctx11.lineCap = 'round';
				
				ctx12.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx12.fillStyle = '#F6D565';
				ctx12.lineCap = 'round';

				ctx21.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx21.fillStyle = '#3A5E8C';
				ctx21.lineCap = 'round';

				ctx22.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx22.fillStyle = '#3A5E8C';
				ctx22.lineCap = 'round';
				
				// Draw rectangle for each frequency bin.
				for (var i = 0; i < numBars; ++i) {
					var magnitude = freqByteData[i + OFFSET] / 3;
					if(i % 2 == 0) {
						ctx11.fillStyle = '#ff7648';
						ctx12.fillStyle = '#ff7648';
						ctx21.fillStyle = '#637a94';
						ctx22.fillStyle = '#637a94';
					} else {
						ctx11.fillStyle = '#ff6633';
						ctx12.fillStyle = '#ff6633';
						ctx21.fillStyle = '#516b88';
						ctx22.fillStyle = '#516b88';
					}
					ctx11.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
					ctx12.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
					ctx21.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
					ctx22.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
				}
			}

			function onLoad(e) {
				setTimeout(function (){
					try {
						window.AudioContext = window.AudioContext||window.webkitAudioContext;
						context = new AudioContext();
						analyser = context.createAnalyser();
					}
					catch(e) {
						// alert('Opps.. Your browser do not support audio API');
					}
					var source = context.createMediaElementSource(audio);
					source.connect(analyser);
					analyser.connect(context.destination);
					audioDuration = audio.duration;
					rafCallback();
				}, 0);
			}

			// Need window.onload to fire first. See crbug.com/112368.
			window.addEventListener('load', onLoad, false);
			
		})();
		/* [/Drawing equalizer] */
		
		function fireEvent(obj,evt){
			var fireOnThis = obj;
			if( document.createEvent ) {
				var evObj = document.createEvent('MouseEvents');
				evObj.initEvent( evt, true, false );
				fireOnThis.dispatchEvent( evObj );
			} else if( document.createEventObject ) {
				var evObj = document.createEventObject();
				fireOnThis.fireEvent( 'on' + evt, evObj );
			}
		}
		
		window.addEventListener('keydown', function(e) {
			if (e.keyCode == 32) { // space
				// Simulate link click on an element.
				fireEvent(window.playButton,'click')
			}
		}, false);

	}

});


$(function() {

	window.App = window.App || {};
	
	App.openClosePanels = $('.js-icheck-open-close');
	
	App.iChecks = $('.js-icheck');
	
	App.Forms = (function() {

		function initICheck($scope) {

			$scope.find('.js-icheck')
				.on('ifToggled', function() {
					App.Forms.updateIChecksParent($(this));
				})
				.iCheck({
					increaseArea: '20%'
				})
				.map(function(index, elm) {
					var $this = $(elm),
						target = $this.data('open-close-target');
					if( $this.is(':checked') && target && $('#' + target).size() ) {
						// If checked="checked" panel 'open-close-target' opened
						$('#' + target)
							.addClass('open')
							.css({
								'display': 'block'
							})
							.addClass('activate');
					}
				});
			
			if( App.openClosePanels.size() ) {
				
				App.openClosePanels
				.filter(':not(.activate)')
				.css({
					'display': 'block'
				})
				.addClass('activate');
				App.openClosePanels
					.filter('.open')
					.slideDown(0)
					.end()
					.filter(':not(.open)')
					.slideUp(0);
				
			}
		}

		return {

			initElements: function($scope) {
				initICheck($scope);
			},

			updateIChecksParent: function($items) {
				$items.each(function() {
					var $input = $(this);
					var $parent = $input.closest('.js-icheck-parent');
					var checked = $input.prop('checked');
					var $openClosePanel = $("#" + $input.data('open-close-target'));

					if (checked) {
						$parent.addClass('active');
					} else {
						$parent.removeClass('active');
					}
					if($openClosePanel.size()) {
						
						App.openClosePanels
							.not($openClosePanel)
							.slideUp(400)
							.removeClass('open');
						
						if (checked) {
							$openClosePanel
								.slideDown(400)
								.addClass('open');
						} else {
							$openClosePanel
								.slideUp(400)
								.removeClass('open');
						}
					}
				});

			}

		};

	}());

	App.Forms.initElements($('body'));

	$(document).on('shown.bs.modal', '.modal', function() {
		var $scope = $(this);
		App.Forms.initElements($scope);
	});

	// init submit for #example-form-reserve
	$(document).on('submit', '#example-form-reserve', function(e) {
		e.preventDefault();

		var $form = $(this);

		$form.ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function() {
				$form.addClass('form_loading');
			},
			success: function(data) {

				if (data.error == 1) {
                    $form.removeClass('form_loading');

					var $comment = $form.find('.main-short-search__comment');
					var $errorBox = $form.find('.js-error-box');
					
					var $error = $('<div />')
						.addClass('form__error')
						.html(data.errorText);

					$comment.hide();
					$errorBox.html($error);

					$form.find('.form-control').addClass('error');	
					
				}
                else {
                    if (data.redirect != undefined) {
                        document.location = data.redirect;
                    }
                }
			},
			error: function() {
				$form.removeClass('form_loading');
			}

		});
	});

	// init submit for #subscription-form
	$(document).on('submit', '#subscription-form', function(e) {
		e.preventDefault();

		var $form = $(this);

		$form.ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function() {
				$form.addClass('form_loading');
			},
			success: function(data) {

				if (data.error == 1) {
                    $form.removeClass('form_loading');

					var $comment = $form.find('.main-short-search__comment');
					var $errorBox = $form.find('.js-error-box');
					
					var $error = $('<div />')
						.addClass('form__error')
						.html(data.errorText);

					$comment.hide();
					$errorBox.html($error);

					$form.find('.form-control').addClass('error');	
					
				}
                else {
                    if (data.redirect != undefined) {
                        document.location = data.redirect;
                    }
                }
			},
			error: function() {
				$form.removeClass('form_loading');
			}

		});
	});

	// init submit for .js-example-form-modal
	$(document).on('submit', '.js-example-form-modal', function(e) {
		e.preventDefault();

		var $form = $(this);

		$form.ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function() {
				$form.addClass('form_loading');
			},
			success: function(data) {

				if (data.error == 1) {
                    $form.removeClass('form_loading');

					var $errorBox = $form.find('.js-error-box');

					var $error = $('<div />')
						.addClass('form__error')
						.html(data.errorText);

					$errorBox.html($error);
					$error.wrap('<div class="form-group"></div>');

					$form.find('.form-control').addClass('error');	
					
				}
                else {
                    if (data.redirect != undefined) {
                        document.location = data.redirect;
                    }
                }

			},
			error: function() {
				$form.removeClass('form_loading');
			}

		});
	});

	/**
	 * init button for removing input value
	 */
	
	$(document).on('click', '.input__clear', function (e) {
		e.preventDefault();
		
		var $button = $(this),
			$input = $button.siblings('.input_has-clear');

		$input.val('').trigger('change');
	});

	$(document).on('keydown change', '.input_has-clear', function () {
		var $input = $(this),
			$button = $input.siblings('.input__clear');

		setTimeout(function () {
			if ($input.val().length == 0) {
				$button.hide();
			} else {
				$button.show();
			}
		}, 0);
	});

});

$(function() {

	window.App = window.App || {};

	App.Manage = (function() {
			
		$(document).on('ifToggled', '.js-check-all', function() {
			var $scope = $(this).closest('.js-manage-parent');
			var $button = $(this);
			toggleItems($scope, $button);
			updateButtons($scope);
		});

		$(document).on('ifToggled', '.js-check-all-area :checkbox', function() {
			var $scope = $(this).closest('.js-manage-parent');
			updateCtrls($scope);
		});
		
		// check/uncheck all
		function toggleItems($scope, $button) {
			var $items = $scope.find('.js-check-all-area :checkbox');
			var checked = $button.prop('checked');

			$items
				.prop('checked', checked)
				.iCheck('update');
				
			App.Forms.updateIChecksParent($items);
		}

		// update all buttons
		function updateCtrls($scope) {
			var $checkAll = $scope.find('.js-check-all');
			var $items = $scope.find('.js-check-all-area :checkbox');
			var counts = getCountInfo($items);

			if (isAllChecked(counts)) {
				$checkAll
					.prop('checked', true)
					.iCheck('update');
			} else {
				$checkAll
					.prop('checked', false)
					.iCheck('update');
			}

			updateButtons($scope);
		}

		// update action buttons only
		function updateButtons($scope) {
			var $perOneButtons = $scope.find('.js-manage-button[data-target="one"]');
			var $perAllButtons = $scope.find('.js-manage-button[data-target="all"]');
			var $items = $scope.find('.js-check-all-area :checkbox');
			var counts = getCountInfo($items);

			if (isOneChecked(counts)) {
				$perAllButtons.removeClass('hidden');
				$perOneButtons.removeClass('hidden');
			} else if (isAnyChecked(counts)) {
				$perAllButtons.removeClass('hidden');
				$perOneButtons.addClass('hidden');
			} else {
				$perAllButtons.addClass('hidden');
				$perOneButtons.addClass('hidden');
			}
		}

		function getCountInfo($items) {
			var totalCount = $items.size();
			var $checked = $items.filter(':checked');
			var checkedCount = $checked.size();

			return {
				'total': totalCount,
				'checked': checkedCount
			};
		}

		function isOneChecked(counts) {
			return counts.checked == 1; 
		}

		function isAnyChecked(counts) {
			return counts.checked > 0;
		}

		function isAllChecked(counts) {
			return counts.total == counts.checked;
		}

		return {
			update: function() {

				$('.js-manage-parent').each(function() {
					var $scope = $(this);
					updateCtrls($scope);
				});

			}
		}

	}());

}());
/**
 * Number.prototype.format(n, x, s, c)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

jQuery(function($) {

	/**
	 * init slider desc
	 */
	
	(function () {
		var $slider = $('#slider-desc');

		$('.slider__body', $slider).bxSlider({
			nextSelector: $('.slider__next', $slider),
			pagerSelector: $('.slider__pager', $slider),
			prevSelector: $('.slider__prev', $slider),
			prevText: '',
			nextText: ''
		});
	}());

	/**
	 * init slider thumbs
	 */

	(function () {

		var navTemplate = '<div class="slider-thumbs__nav">' + 
				'<div class="slider-thumbs__nav__tube">' +
					'<% _.each(images, function(image, index) { %>' +
						'<% if (index == 0) { %>' + 
							'<a href="#" class="slider-thumbs__nav__item slider-thumbs__nav__item_active">' +
								'<span class="slider-thumbs__nav__item__in">' + 
									'<img src="<%= image.src %>" class="slider-thumbs__nav__image">' +
								'</span>' +
							'</a>' +
						'<% } else { %>' + 
							'<a href="#" class="slider-thumbs__nav__item">' +
								'<span class="slider-thumbs__nav__item__in">' + 
									'<img src="<%= image.src %>" class="slider-thumbs__nav__image">' +
								'</span>' +
							'</a>' +
						'<% } %>' +
					'<% }); %>' +
				'</div>' +
			'</div>' + 
			'<a href="#" class="slider-thumbs__menu"></a>';

		var $sliders = $('.slider-thumbs');

		$sliders.each(function() {

			var $slider = $(this);
			var uniqId = Math.random().toString(36).substring(7);
			var $images = $slider.find('img');
			var navData = {images: []};

			// fil navData

			$images.each(function() {
				navData.images.push({ src: $(this).attr('src') });
			});

			// create structure

			$slider.wrapInner('<div class="slider-thumbs__body"></div>');
			$slider.find('img').each(function() {
				var src = $(this).attr('src');

				$(this).wrap('<a href="' + src + 
					'" data-fancybox-group="' + 
					uniqId +'"></a>');
			});
			$slider.find('a').wrap('<div class="slider-thumbs__item"></div>');

			var navHtml = _.template(navTemplate)(navData);
			$(navHtml).prependTo($slider);

			// init slider
			
			var bxSlider = $slider.find('.slider-thumbs__body').bxSlider({
				pager: false,
				controls: false,
				adaptiveHeight: true,
				captions: true,
				mode: 'fade'
			});

			$slider.data('bxSlider', bxSlider);
			
		});

		$(document).on('click', '.slider-thumbs__menu', openFancybox);
		$(document).on('click', '.slider-thumbs__nav__item', switchSlide);
		$sliders.find('a[data-fancybox-group]').fancybox();

		// try keep active item in middle
		function fixNavScroll($slider) {

			var menuHeight = 60;
			var $box = $slider.find('.slider-thumbs__nav');
			var $tube = $box.find('.slider-thumbs__nav__tube');
			var boxHeight = $box.height() - menuHeight;
			var tubeHeight = $tube.height();

			if (boxHeight >= tubeHeight) return;

			var $items = $box.find('.slider-thumbs__nav__item');
			var $active = $items.filter('.slider-thumbs__nav__item_active');
			var index = $active.index();
			var itemHeight = $items.height();
			var itemsGap = parseInt($items.css('margin-bottom'));
			var activePos = Math.ceil(boxHeight/(itemHeight + itemsGap)/2);

			var maxOffset = tubeHeight - boxHeight;
			var minOffset = 0;

			var offset = (index+1-activePos)*(itemHeight+itemsGap);
			if (offset > maxOffset) offset = maxOffset;
			else if (offset < minOffset) offset = minOffset;

			$tube.stop(true).animate({
				marginTop: -offset
			}, 'fast');

		}

		function openFancybox(e) {
			e.preventDefault();

			$(this)
				.closest('.slider-thumbs')
				.find('.slider-thumbs__item:visible a')
				.trigger('click');
		}

		function switchSlide(e) {
			e.preventDefault();

			var $slider = $(this).closest('.slider-thumbs');
			var bxSlider = $slider.data('bxSlider');
			var index = $(this).index();

			$(this)
				.siblings('.slider-thumbs__nav__item_active')
				.removeClass('slider-thumbs__nav__item_active')
				.end()
				.addClass('slider-thumbs__nav__item_active');

			bxSlider.goToSlide(index);
			fixNavScroll($slider);
		}

	}()); 

	/**
	 * init nav tour
	 */

	(function () {

		var slider,
			$slider = $('#nav-tour .nav-tour__in'),
			$activeItem = $slider.find('.nav-tour__link.active').closest('.nav-tour__item'),
			targetIndex = ($activeItem.size()) ? $activeItem.index() : 0,
			count = $slider.find('.nav-tour__item').size(),
			visible = 6,
			maxStart = count - visible,
			start = targetIndex - 1;

		if (!$slider.size() || count <= visible) return;

		if (start < 0) start = 0;
		else if (start > maxStart) start = maxStart;

		slider = $slider.lightSlider({
			item: visible,
			slideMargin: 0,
			useCSS: false,
			pager: false
		});

		slider.goToSlide(start);

	}());
	

	/**
	 * init faq collapsing
	 */

	(function () {

		$('.js-faq-collapse').on('show.bs.collapse hide.bs.collapse', function() {
			$(this).closest('.faq').toggleClass('faq_open');
		});

	}());

	/**
	 * init raty
	 */

	(function () {

		$('.js-raty').raty({
			space: false,
			starOff: 'images/star-off.png',
			starOn: 'images/star-on.png',
			scoreName: function() {
				return $(this).data('score-name');
			}
		});

	}());

	/**
	 * init fancybox
	 */
	
	(function () {

		$('.js-fancybox').fancybox();

	}());

	/**
	 * init popover
	 */

	(function () {

		$('.js-popover').popover();

		$(document).on('click', '.popover-close', function(e) {
			e.preventDefault();
			$(this).closest('.popover').popover('hide');
		});

		$(document).on('click', '.js-popover', function(e) {
			e.preventDefault();
		});

	}());
	

	/**
	 * init datetimepicker
	 */

	(function() {

		// [Block for each individual calendar]
		var $parent = $(".js-datetimepicker")
			uniqIdFromTo = 0,
			uniqIdFrom = 0,
			uniqIdTo = 0; // Unique id for from/to calendar diapason
		
		// If Archive href
		if($parent.hasClass('archive')) {
			$parent.find('form').addClass('archive__form').removeClass('dnone').find('.btn').removeClass('dnone');
			$parent.on('click', function(e) {
				e.preventDefault();
				$parent.addClass('open');
			});
			$parent.find('.btn').on('click', function(e) {
				e.preventDefault();
					
				return false;
			});
		}
		
		// [The format of the dates displayed in the input field]
		$.datepicker._defaults.dateFormat = 'dd.mm.yy' ;
		
		// [Update the design of the selectors in the calendar and add Close button]
		var timePickerCustomSelect = function(obj) {
			if( obj.find('.select-wrapper').size() == 0 ) { 
				obj.find('select').wrap("<div class='select-wrapper'></div>");
				$("<a class='close-datepicker' href='#'></a>").insertAfter( obj.find('.select-wrapper:last') );
			}
			return true;
		}
		
		var createSimpleCalendar = function(params) {
			
			var uniqIdFromTo = (typeof params.uniqIdFromTo !== 'undefined')? params.uniqIdFromTo : 0 ;
			var indxCal = params.indxCal;
			
			if(typeof params.daterange1 !== 'undefined' && typeof params.daterange2 !== 'undefined') {
				params.$calendar.datepicker({
					autoclose: false,
					changeMonth: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					changeYear: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					yearRange : '1910:2050',
					minDate : (params.minDate)? params.minDate : null,
					maxDate : (params.maxDate)? params.maxDate : null,
					defaultDate : null,
					beforeShowDay: function(date) {
						var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange1.val());
						var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange2.val());
						return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
					},
					onSelect: function(dateText, inst) {
						var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange1.val());
						var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, params.daterange2.val());
						if (!date1 || date2) {
							params.daterange1.val(dateText);
							params.daterange2.val("");
							$(this).datepicker("option", "minDate", dateText);
						} else {
							params.daterange2.val(dateText);
							$(this).datepicker("option", "minDate", null);
						}
						
						if(params.daterange2.val() != '' && params.daterange1.val() != '') {
							var changeDateEvent = $.Event('change.dbl.date');
							$(this).trigger(changeDateEvent);
						}

						// [If both are selected in the calendar with the date range, you can close it]
						params.form_control.val( (params.daterange2.val() != '' && params.daterange1.val() != '')? params.daterange1.val() + " - " + params.daterange2.val() : params.daterange1.val() );
						
						setTimeout(function() {
							params.$calendar.find("td.dp-highlight:eq(0)>a").addClass("ui-state-active");
							timePickerCustomSelect(params.$calendar);
						}, 5);
					},
					onClose:function(selectedDate) {
						$.datepicker._datepickerShowing = false;
					},
					onChangeMonthYear: function(year, month, inst) {
						// Update the design of the selectors in the calendar
						setTimeout(function() {
							timePickerCustomSelect(params.$calendar);
						}, 5);
					}
				});
		
			} else {
				
				// [Calendar one date for select year/month if exist class js-datetimepicker-born]
				params.$calendar.datepicker({
					autoclose: false,
					changeMonth: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					changeYear: ( params.$calendar.hasClass('js-datetimepicker-born') )? true : false,
					yearRange : '1910:2050',
					minDate : (params.minDate)? params.minDate : null,
					maxDate : (params.maxDate)? params.maxDate : null,
					defaultDate : (params.minDate && uniqIdFromTo && uniqIdFromTo.indexOf("to") >= 0)? params.minDate : (params.maxDate && uniqIdFromTo && uniqIdFromTo.indexOf("from") >= 0)? params.maxDate : null,
					onSelect: function(dateText, inst) {
						if(typeof params.form_control != 'undefined') {
							params.form_control.val(dateText);
						}
						// Update the design of the selectors in the calendar
						setTimeout(function() {
							timePickerCustomSelect(params.$calendar);
						}, 5);
						
						var countsNight = 1;
						
						if(params.minDate && params.maxDate) {
							var countsNight = (new Date(params.maxDate).getTime() - new Date(params.minDate).getTime()) / 86400000;
						}
						
						if(uniqIdFromTo) {
							var minMaxDate = (uniqIdFromTo.indexOf("from") >= 0)? "maxDate" : "minDate";
							
							var date_arr = dateText.split('.'),
								toDate = new Date(Date.parse(date_arr[2] + '.' + date_arr[1] + '.' + date_arr[0] )),
								oneDay = (minMaxDate == 'maxDate')? new Date(toDate.getTime() - 86400000) : new Date(toDate.getTime() + 86400000);

							$("#" + uniqIdFromTo)
												.datepicker( "option", minMaxDate, oneDay );

							if(minMaxDate == 'minDate') {
								countsNight = ($("#" + uniqIdFromTo).datepicker( "getDate" ).getTime() - $("#" + uniqIdFromTo.replace('to','from')).datepicker( "getDate" ).getTime()) / 86400000;
							} else {
								countsNight = ($("#" + uniqIdFromTo.replace('from','to')).datepicker( "getDate" ).getTime() - $("#" + uniqIdFromTo).datepicker( "getDate" ).getTime()) / 86400000;
							}

							// Add change selected range date
							var $nightCount = $('#' + params.form_control.data("night-count-target")),
								$nightPrice = $('#' + params.form_control.data("night-price-target"));

							if($nightCount.size()) {
								if($nightCount.is("select")) {
									if($nightCount.find("option[value='" + countsNight  + "']").size() > 0) {
										$nightCount
											.find("option")
											.prop('selected', false)
											.removeAttr('selected')
											.filter("[value='" + countsNight  + "']")
											.attr('selected','selected')
											.prop('selected', true);
									} else {
										$nightCount
											.find("option")
											.removeAttr('selected')
											.prop('selected', false);
										$nightCount
											.append($("<option></option>")
											.attr('selected','selected')
											.prop('selected', true)
											.attr("value",countsNight)
											.text(countsNight));
										
									};
								} else {
									$nightCount.val(countsNight);
								}
							}
							
							if($nightPrice.size() && $nightPrice.data('base-price')) {
								var price = $nightPrice.data('base-price') * countsNight;
								$nightPrice.html( price.format(0, 3, ' ', '.') );
							}
							
						}	

					},
					onClose:function(selectedDate) {
						$.datepicker._datepickerShowing = false;
					},
					onChangeMonthYear: function(year, month, inst) {
						// Update the design of the selectors in the calendar
						setTimeout(function() {
							timePickerCustomSelect(params.$calendar);
						}, 5);
					}
				});
				

				// SetDefault Nights count
				var $nightCount = $('#' + params.form_control.data("night-count-target")),
					$nightPrice = $('#' + params.form_control.data("night-price-target"));

				if(params.minDate && params.maxDate && $nightCount.size()) {
					var arr_min = params.minDate.split('.'),
						toDateMin = new Date(Date.parse(arr_min[2] + '-' + arr_min[1] + '-' + arr_min[0] )).getTime(),
						arr_max = params.maxDate.split('.'),
						toDateMax = new Date(Date.parse(arr_max[2] + '-' + arr_max[1] + '-' + arr_max[0] )).getTime();
					var countsNight = (toDateMax - toDateMin) / 86400000;
					$nightCount.val(countsNight);
					var price = $nightPrice.data('base-price') * countsNight;
					$nightPrice.html( price.format(0, 3, ' ', '.') );
					
				}

				
				
			}
		}
		
		var calendarsCycle = function(params) {

			var $calendar = $(".js-datetimepicker-calendar", params.$this), // span, into calendar parse
				daterange1 = $(".js-datetimepicker-date-1", params.$this), // Hidden field for first-date
				daterange2 = $(".js-datetimepicker-date-2", params.$this), // Hidden field for two-date, for dates diapazon
				form_control = $(".js-datetimepicker-input", params.$this), // Input field, when you click on the calendar that displays
				form_control_add = $(".js-datetimepicker-icon", params.$this), // Calendar icon, when you click on the calendar that displays
				uniqId = Math.random().toString(36).substring(7), // Unique ID
				minDate = null,
				maxDate = null;

				if( params.$this.hasClass('js-datetimepicker-from') ) {
					uniqIdFromTo = uniqId;
					params.$this
						.find('.js-datetimepicker-calendar')
						.attr('id', uniqIdFromTo + '-from')
					params.$this
						.find('input[type="text"]')
						.addClass(uniqIdFromTo + '-from');
					uniqIdTo = uniqIdFromTo + '-to';
					uniqIdFrom = 0;
					
				} else if( params.$this.hasClass('js-datetimepicker-to') ) {
					if(uniqIdFromTo) {
						params.$this
							.find('.js-datetimepicker-calendar')
							.attr('id', uniqIdFromTo + '-to');
						params.$this.find('input[type="text"]')
							.addClass(uniqIdFromTo + '-to');
						uniqIdFrom = uniqIdFromTo + '-from';
						uniqIdFromTo = 0;
						uniqIdTo = 0;
					}
				} else {
					params.$this
								.attr('data-id', uniqId);
					uniqIdFrom = 0;
					uniqIdTo = 0;
				}
				
				if (typeof params.refresh != 'undefined' && params.refresh) {
					$calendar.removeClass("hasDatepicker").html("").attr("id", "");
					daterange1.val("");
					daterange2.val("");
					form_control.val("");
				}

				
				if(params.$this.find('input[type="text"]').data('max-date')) {
					maxDate = params.$this.find('input[type="text"]').data('max-date');
				} else if(params.$this.data('max-date')) {
					maxDate = params.$this.data('max-date');
				} else {
					maxDate = null;
				}
				if(params.$this.find('input[type="text"]').data('min-date')) {
					minDate = params.$this.find('input[type="text"]').data('min-date');
				} else if(params.$this.data('min-date')) {
					minDate = params.$this.data('min-date');
				} else {
					minDate = null;
				}

		
				// [Hide calendar, if click outside the calendar]
				$(document).on('click', 'html,body', function(e) {
					var $target = $(e.target);

					if ( $target.closest('.js-datetimepicker-calendar').size() == 0 ) {
						$('.hasDatepicker', params.$this).hide();
						if($parent.hasClass('archive')) {
							$parent.removeClass('open');
						}
					}
				});

			// [Diapazon date calendars]
			if(daterange1.size() > 0 && daterange2.size() > 0) {
				
				createSimpleCalendar({
										'$this':params.$this,
										'$calendar':$calendar,
										'form_control':form_control,
										'daterange1':daterange1,
										'daterange2':daterange2,
										'minDate': minDate,
										'maxDate': maxDate
									});
							
			
			} else {// [One date calendars]
				
				// [Calendar one date for select year/month if exist class js-datetimepicker-born]
				
				createSimpleCalendar({
										'$this':params.$this,
										'$calendar':$calendar,
										'form_control':form_control,
										'uniqIdFromTo': uniqIdTo || uniqIdFrom || 0,
										'minDate': minDate,
										'maxDate': maxDate
									});
			
			}
			// [Hide calendars after parsing]
			$('.hasDatepicker').hide();
		}
		
		// [Open calendar on click into input or calendar icon]
		$(document).on("click focus", ".js-datetimepicker-input, .js-datetimepicker-icon", function(e) {
			e.preventDefault();
				var $this = $(this).closest(".js-datetimepicker"),
					$calendar = $(".js-datetimepicker-calendar", $this),
					dataId = $this.attr("data-id"),
					$elms = $(".js-datetimepicker[data-id='" + dataId + "']"),
					index = $elms.index($this);
				$('.hasDatepicker').hide();
				$('.hasDatepicker', $this).show();
				// Castomize select in calendar
				if(index > 0) {
					// Refresh after cloning
					calendarsCycle({'$this': $this, 'refresh': 1});
				}
				setTimeout(function() {
					timePickerCustomSelect($calendar);
				}, 5);
			return false;
		});
		
		// [Calendar close, if close cross exist]
		$(document).on('mousedown', '.close-datepicker', function(e) {
			e.preventDefault();
				$(this).closest('.hasDatepicker').removeClass('open').hide();
				$(this).closest('.archive').removeClass('open');
			return false;
		});
		
		// [Parse static calendars]
		$parent.each(function(index) {
			var $this = $(this);
			calendarsCycle({
							'$this': $this,
							'$parent': $parent
						});
		});
		
		// [Create calendars on open modal window]
		$(".modal").on('shown.bs.modal loaded.bs.modal', function (e) {
			var $parent = $(".js-datetimepicker", $(this));
			$parent.each(function(index) {
				var $this = $(this);
				calendarsCycle({
					'$this': $this,
					'$parent': $parent
				});
			});
		});

	}());
	
	// [Cloned calendar sample]
	$(document).on('click', '.js-datetimepicker-clone', function(e) {
		e.preventDefault();
		var $this = $(this),
			$calendar = $this.prev('.js-datetimepicker:eq(0)');
		$calendar.clone(true,true).insertAfter($calendar);
	});
	
	// [Collapsed block in form]
	$(document).on('ifChecked ifUnchecked', '.js-icheck', function(e) {
		$(this).closest('a').trigger('click');
	});

	
	$('form.js-form-validate,form').each(function() {
		var $this = $(this),
			$inputs = $("input.required,textarea.required,select.required"),
			$btn = $(".btn[type='submit'],button[type='submit']",$this),
			$comment = $(".comment-error",$this);
		$this.validate({
			errorPlacement: function(error,element) {
				return true;
			}
		});
		$this.submit(function(e) {
			$('.datetimepicker,.select-wrapper-full').has('input.error,select.error').addClass('error').end().has('input.valid,select.valid').removeClass('error');
			if($(this).valid()) {
				$btn
					.removeClass('button_disabled')
					.removeAttr('disabled');
				$comment.hide();
				return true;
			} else {
				e.preventDefault();
				$btn
					.addClass('button_disabled')
					.attr('disabled', 'disabled');
				$comment.show();
				return false;
			}
		});
		$(document).on('keyup change select focusout', "input.required,textarea.required,select.required", function(e) {
			setTimeout(function() {
				$('.datetimepicker,.select-wrapper-full').not(':has(input.error,select.error)').removeClass('error');
			}, 5);
			
			if($this.valid()) {
				$btn
					.removeClass('button_disabled')
					.removeAttr('disabled');
				
				$comment.hide();
			}
		});

	});

	
	// [Add review]
	(function() {

		$(document).on('click', '.js-add-review', function(e) {
			e.preventDefault();
			
			$buttonBox = $(this).closest('.add-review-form');
			$form = $buttonBox.next('.review-form');

			$buttonBox.slideUp();
			$form.slideDown();
		});

		var readmore = $('.readmore'),
			showmore = readmore.prev('.show-more');
		
		showmore.slideUp(0);
		
		readmore.click(function(e) {
			var $this = $(this),
				$this_text = $this.text();
				$this_data = $this.data('title'),
				showmore = $(this).prev('.show-more'),
				shadow = showmore.prev('.shadow');
			
			shadow.toggle();
			if(showmore.size()) {
				e.preventDefault();
				showmore.slideToggle(200);
				$this.data('title',$this_text).text($this_data);
			}
		});
		
	}());
	// [Add review]

	
	// [Checkform]
	(function() {
		
		$('.subscribe-form').submit(function(e) {
			if($(this).valid()) {
				$(this).removeClass('form-error');
				return true;
			} else {
				e.preventDefault();
				$(this).addClass('form-error');
				return false;
			}
		});

	}());
	// [/Checkform]
	
	/**
	 * Kids Age and Custom scrollbar
	 */
	(function() {
		
		$(".bootstrap-select div.dropdown-menu:eq(0)")
			.css({
				'height': 150
			})
			.mCustomScrollbar();

	}());


	/**
	 * init nav fixed
	 */
	
	(function() {
		
		var $navFixed = $('#nav-fixed');

		// If person closes navigation when it doen't disturb him,
		// we switch to manual mode. In manual mode we don't 
		// try show navigation automatically on wide screen.
		var manualMode = false;
		var manualTimer;
		var lastWindowWidth = $(window).width();
		var breakpoint = 1500;

		$(document).on('click', '.js-nav-fixed-ctrl', toggleNav);
		$(window).resize(showNavOnWideScreen);

		function toggleNav(e) {
			e.preventDefault();

			if (isNavVisible()) {
				hideNav();				
			} else {
				showNav();
			}
		}

		function isNavVisible() {
			var marginLeft = parseInt($navFixed.css('margin-left'));
			return (marginLeft == 0) ? true : false;
		}

		function hideNav() {
			$navFixed
				.removeClass('fixed-pane_show')
				.addClass('fixed-pane_hide');

			if (manualMode) return;

			if ($(window).width() >= breakpoint) {
				enableManual();
			}
		}

		function showNav() {
			$navFixed
				.removeClass('fixed-pane_hide')
				.addClass('fixed-pane_show');

			if (manualMode) return;
			
			preventManual();	
		}

		// Set default state, if:
		// - we resize across breakpoint
		// - nav is closed when resize
		function showNavOnWideScreen() {
			if (manualMode) return;

			var windowWidth = $(window).width();

			if (!isNavVisible() && lastWindowWidth < breakpoint
					 && windowWidth >= breakpoint) {
				setDefault();
			}

			lastWindowWidth = windowWidth;
		}

		function setDefault() {
			$navFixed
				.removeClass('fixed-pane_show fixed-pane_hide');
		}

		function enableManual() {
			manualTimer = setTimeout(function() {
				manualMode = true;
			}, 2000);
		}

		function preventManual() {
			clearTimeout(manualTimer); 
		}

	}());

	/**
	 * init autosize
	 */
	
	(function() {
		autosize($('textarea'));
	}());

	/**
	 * init select2
	 */
	
	(function() {

		$('.js-select-person').each(function() {
			var url = $(this).data('url'),
				$this = $(this);

			$(this).select2({
				closeOnSelect: false,
				ajax: {
					delay: 250,
					cache: false,
					url: url,
					dataType: 'json',
					processResults: function (data) {
						return {
							results: data.items
						};
					}
				},
				templateResult: function(person) {
					if (!person.id ) { return person.text; }
					
					if($this.hasClass('js-select-person-noavatar')) {
						var $person = $(
							'<div class="select2__person">' +
								'<div class="select2__person__name_noimage">' +
									person.text +
								'</div>' +
							'</div>'
						);
					} else {
						var $person = $(
							'<div class="select2__person">' +
								'<div class="select2__person__image" style="background-image:url(' +
									person.imageSrc +
								');">' +
								'</div>' +
								'<div class="select2__person__name">' +
									person.text +
								'</div>' +
							'</div>'
						);
					}
					
					return $person;
				}
			});
		});

		$('.js-select:not([multiple])').select2({
			minimumResultsForSearch: Infinity,
			allowClear: true
		});	
		$('.js-select[multiple]').select2({
			minimumResultsForSearch: Infinity,
			allowClear: false
		});	
		
	}());


	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
	
	/**
	* Forms collapsed block
	*/
	(function(){
		
		var $collapsedHref = $('.js-form__collapsed-href'),
			$collapsedBlock = $('.js-form__collapsed-block');
		$collapsedBlock.slideUp(0);
		
		$collapsedHref.on('click', function(e) {
			e.preventDefault();
			var $this = $(this),
				index = $collapsedHref.index($this);
			$collapsedBlock.eq(index)
				.slideToggle(400)
				.toggleClass('open');
			$this
				.toggleClass('active');
			return false;
		});
		
	}());
	
	/**
	* Dynamic accordion block
	*/
	(function(){
		
		var $accordions_list = $('accordion').wrapAll('<div class="accordions-list"></div>'),
			$parents_accordion = $('.accordions-list');
		$parents_accordion.each(function(index) {
			var $this = $(this),
				$accordions = $this.find('accordion'),
				$parent_accordion = $('<div/>').attr('id', 'accordion-' + index)
			$this.html('');
			$accordions.each(function(ind) {
				var $accordion = $(this),
					title = $accordion.attr('title'),
					content = $accordion.html(),
					open = ($accordion.attr('open'))? true : false,
					$accordion__item = $('<article/>')
													.addClass('accordion__item panel'),
					
					$h2 = $('<h2/>')
									.addClass('reset-font margin-bottom-0'),
					
					$a = $('<a/>')
								.attr({
									'href': '#accordion-' + index + '-' + ind,
									'data-toggle': 'collapse',
									'data-parent': '#accordion-' + index
								})
								.addClass( (open)? 'accordion__ctrl' : 'accordion__ctrl collapsed' )
								.text( title ),
					
					$collapse = $('<div/>')
										.addClass( (open)? 'collapse in' : 'collapse' )
										.attr('id', 'accordion-' + index + '-' + ind),
					
					$accordion__body = $('<div/>')
											.addClass('accordion__body'),
					
					$article = $('<div/>')
										.addClass('article')
										.html( content );

					$h2.append( $a );
					$accordion__body.append( $article );
					$collapse.append( $accordion__body );
					$accordion__item.append( $h2 ).append( $collapse );
					$parent_accordion.append($accordion__item);
			});
			$this.append($parent_accordion);
		});
		
	}());
	
	
});
