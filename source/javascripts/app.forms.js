
$(function() {

	window.App = window.App || {};
	
	App.openClosePanels = $('.js-icheck-open-close');
	
	App.iChecks = $('.js-icheck');
	
	App.Forms = (function() {

		function initICheck($scope) {

			$scope.find('.js-icheck')
				.on('ifToggled', function() {
					App.Forms.updateIChecksParent($(this));
				})
				.iCheck({
					increaseArea: '20%'
				})
				.map(function(index, elm) {
					var $this = $(elm),
						target = $this.data('open-close-target');
						
					if( $this.is(':checked') && target && $('#' + target).size() ) {
						// If checked="checked" panel 'open-close-target' opened
						$('#' + target)
							.addClass('open')
							.css({
								'display': 'block'
							})
							.addClass('activate');
					}
					
					App.Forms.updateIChecksParent($this);
				});
			
			if( App.openClosePanels.size() ) {
				
				App.openClosePanels
				.filter(':not(.activate)')
				.css({
					'display': 'block'
				})
				.addClass('activate');
				App.openClosePanels
					.filter('.open')
					.slideDown(0)
					.end()
					.filter(':not(.open)')
					.slideUp(0);
				
			}
		}

		return {

			initElements: function($scope) {
				initICheck($scope);
			},

			updateIChecksParent: function($items) {
				$items.each(function() {
					var $input = $(this);
					var $parent = $input.closest('.js-icheck-parent');
					var checked = $input.prop('checked');
					var disabled = $input.prop('disabled');
					var $openClosePanel = $("#" + $input.data('open-close-target'));

					if (checked) {
						$parent.addClass('active');
					} else {
						$parent.removeClass('active');
					}
					if (disabled) {
						$parent.addClass('disabled');
					} else {
						$parent.removeClass('disabled');
					}
					if($openClosePanel.size()) {
						
						App.openClosePanels
							.not($openClosePanel)
							.slideUp(400)
							.removeClass('open');
						
						if (checked) {
							$openClosePanel
								.slideDown(400)
								.addClass('open');
						} else {
							$openClosePanel
								.slideUp(400)
								.removeClass('open');
						}
					}
				});

			}

		};

	}());

	App.Forms.initElements($('body'));

	$(document).on('shown.bs.modal', '.modal', function() {
		var $scope = $(this);
		App.Forms.initElements($scope);
	});

	// init submit for #example-form-reserve
	$(document).on('submit', '#example-form-reserve', function(e) {
		e.preventDefault();

		var $form = $(this);

		$form.ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function() {
				$form.addClass('form_loading');
			},
			success: function(data) {
				$form.removeClass('form_loading');

				if (data.error == 1) {

					var $comment = $form.find('.main-short-search__comment');
					var $errorBox = $form.find('.js-error-box');
					
					var $error = $('<div />')
						.addClass('form__error')
						.html(data.errorText);

					$comment.hide();
					$errorBox.html($error);

					$form.find('.form-control').addClass('error');	
					
				}
			},
			error: function() {
				$form.removeClass('form_loading');
			}

		});
	});
	// init submit for #subscription-form
	$(document).on('submit', '#subscription-form', function(e) {
		e.preventDefault();

		var $form = $(this);

		$form.ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function() {
				$form.addClass('form_loading');
			},
			success: function(data) {
				$form.removeClass('form_loading');

				if (data.error == 1) {

					var $comment = $form.find('.main-short-search__comment');
					var $errorBox = $form.find('.js-error-box');
					
					var $error = $('<div />')
						.addClass('form__error')
						.html(data.errorText);

					$comment.hide();
					$errorBox.html($error);

					$form.find('.form-control').addClass('error');	
					
				}
			},
			error: function() {
				$form.removeClass('form_loading');
			}

		});
	});

	// init submit for .js-example-form-modal
	$(document).on('submit', '.js-example-form-modal', function(e) {
		e.preventDefault();

		var $form = $(this);

		$form.ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function() {
				$form.addClass('form_loading');
			},
			success: function(data) {
				$form.removeClass('form_loading');

				if (data.error == 1) {

					var $errorBox = $form.find('.js-error-box');

					var $error = $('<div />')
						.addClass('form__error')
						.html(data.errorText);

					$errorBox.html($error);
					$error.wrap('<div class="form-group"></div>');

					$form.find('.form-control').addClass('error');	
					
				}
			},
			error: function() {
				$form.removeClass('form_loading');
			}

		});
	});

	/**
	 * init button for removing input value
	 */
	
	$(document).on('click', '.input__clear', function (e) {
		e.preventDefault();
		
		var $button = $(this),
			$input = $button.siblings('.input_has-clear');

		$input.val('').trigger('change');
	});

	$(document).on('keydown change', '.input_has-clear', function () {
		var $input = $(this),
			$button = $input.siblings('.input__clear');

		setTimeout(function () {
			if ($input.val().length == 0) {
				$button.hide();
			} else {
				$button.show();
			}
		}, 0);
	});



	$('[data-hascontent="true"]').each(function(){
		var $button = $(this),
			$content = $('[data-button="#'+$button.attr('id')+'"]');
		$button.on('ifChecked', function(){
			$content.addClass('active');
		});
		$button.on('ifUnchecked', function(){
			$content.removeClass('active');
		});
	});


});