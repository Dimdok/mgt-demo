/*
 * Minimal classList shim for IE 9
 * By Devon Govett
 * MIT LICENSE
 */
 
 
if (!("classList" in document.documentElement) && Object.defineProperty && typeof HTMLElement !== 'undefined') {
    Object.defineProperty(HTMLElement.prototype, 'classList', {
        get: function() {
            var self = this;
            function update(fn) {
                return function(value) {
                    var classes = self.className.split(/\s+/),
                        index = classes.indexOf(value);
 
                    fn(classes, index, value);
                    self.className = classes.join(" ");
                }
            }
 
            var ret = {                    
                add: update(function(classes, index, value) {
                    ~index || classes.push(value);
                }),
 
                remove: update(function(classes, index) {
                    ~index && classes.splice(index, 1);
                }),
 
                toggle: update(function(classes, index, value) {
                    ~index ? classes.splice(index, 1) : classes.push(value);
                }),
 
                contains: function(value) {
                    return !!~self.className.split(/\s+/).indexOf(value);
                },
 
                item: function(i) {
                    return self.className.split(/\s+/)[i] || null;
                }
            };
            
            Object.defineProperty(ret, 'length', {
                get: function() {
                    return self.className.split(/\s+/).length;
                }
            });
 
            return ret;
        }
    });
}

$(function () {

	if(document.getElementById('js-audio')) {
		/* [Drawing buttons] */
		(function() {
			
			CanvasRenderingContext2D.prototype.line = function(x1, y1, x2, y2) {
				this.lineCap = 'round';
				this.beginPath();
				this.moveTo(x1, y1);
				this.lineTo(x2, y2);
				this.closePath();
				this.stroke();
			}
			CanvasRenderingContext2D.prototype.circle = function(x, y, r, fill_opt) {
				this.beginPath();
				this.arc(x, y, r, 0, Math.PI * 2, true);
				this.closePath();
				if (fill_opt) {
					this.fillStyle = 'rgba(0,170,187,1)';
					this.fill();
					this.stroke();
				} else {
					this.stroke();
				}
			}
			CanvasRenderingContext2D.prototype.rectangle = function(x, y, w, h, fill_opt) {
				this.beginPath();
				this.rect(x, y, w, h);
				this.closePath();
				if (fill_opt) {
					this.fillStyle = 'rgba(0,170,187,1)';
					this.fill();
				} else {
					this.stroke();
				}
			}
			CanvasRenderingContext2D.prototype.triangle = function(p1, p2, p3, fill_opt) {
				// Stroked triangle.
				this.beginPath();
				this.moveTo(p1.x, p1.y);
				this.lineTo(p2.x, p2.y);
				this.lineTo(p3.x, p3.y);
				this.closePath();
				if (fill_opt) {
					this.fillStyle = 'rgba(0,170,187,1)';
					this.fill();
				} else {
					this.stroke();
				}
			}
			CanvasRenderingContext2D.prototype.clear = function() {
				this.clearRect(0, 0, this.canvas.clientWidth, this.canvas.clientHeight);
			}

			var canvas = document.getElementById('js-audio__playbutton');
			var ctx = canvas.getContext('2d');
			ctx.strokeStyle = 'rgba(0,170,187,1)';
			ctx.lineWidth = 3;

			var R = canvas.width / 2;
			var STROKE_AND_FILL = false;

			canvas.addEventListener('mouseover', function(e) {
				if (this.classList.contains('playing')) {
					drawPauseButton(STROKE_AND_FILL);
				} else {
					drawPlayButton(STROKE_AND_FILL);
				}
				ctx.save();
				ctx.lineWidth += 0;
				ctx.circle(R, R, R - ctx.lineWidth + 1);
				ctx.restore();
			}, true);

			canvas.addEventListener('mouseout', function(e) {
				if (this.classList.contains('playing')) {
					drawPauseButton(STROKE_AND_FILL);
				} else {
					drawPlayButton(STROKE_AND_FILL);
				}
			}, true);

			canvas.addEventListener('click', function(e) {
				this.classList.toggle('playing');
				if (this.classList.contains('playing')) {
					drawPauseButton(STROKE_AND_FILL);
					audio.play();
				} else {
					drawPlayButton(STROKE_AND_FILL);
					audio.pause();
				}
			}, true);

			function drawPlayButton(opt_fill) {
				ctx.clear();
				ctx.circle(R, R, R - ctx.lineWidth + 1, opt_fill);
				ctx.triangle({x: R*0.8, y: R*0.56}, {x: R*1.45, y: R}, {x: R*0.8, y: R*1.45}, true);
			}

			function drawPauseButton(opt_fill) {
				ctx.clear();
				ctx.circle(R, R, R - ctx.lineWidth + 1, opt_fill);
				ctx.save();
				ctx.lineWidth += 0;
				ctx.line(R*0.8, R/2, R*0.8, R*1.5);
				ctx.line(R+(R/5), R/2, R+(R/5), R*1.5);
				ctx.restore();
			}
			
			drawPlayButton(STROKE_AND_FILL);

			window.playButton = canvas;
		
		})();
		/* [/Drawing buttons] */
		
		/* [Drawing equalizer] */
		(function() {
			var canvas11 = document.getElementById('fft11'),
				ctx11 = canvas11.getContext('2d');
				// canvas11.width = document.body.clientWidth / 1.4,
				canvas12 = document.getElementById('fft12'),
				ctx12 = canvas12.getContext('2d');
				// canvas11.width = document.body.clientWidth / 1.4
				;
			var audioDuration = 0;
			var canvas21 = document.getElementById('fft21'),
				ctx21 = canvas21.getContext('2d');
				// canvas21.width = canvas11.width,
				canvas22 = document.getElementById('fft22'),
				ctx22 = canvas22.getContext('2d');
				// canvas22.width = canvas11.width
				;
			var audioPane = document.getElementById("js-audio__pane");
			var audioPaneDefault = document.getElementById("js-audio__pane__default");
			var colorLine = document.getElementById("js-audio__pane__colorline");
			var jsAudio = document.getElementById("js-audio");
			var context,
				analyser;
			var currenTimeNode = document.querySelector('#audio__pane__current-time');
			
			var CANVAS_HEIGHT = canvas11.height;
			var CANVAS_WIDTH = canvas11.width;

			window.audio = new Audio();
			audio.src = jsAudio.getAttribute('data-audio');
			audio.controls = true;
			// audio.autoplay = true;
			audio.loop = true;
			
			// [���������� �������� �������]
			audio.addEventListener('timeupdate', function(e) {
				var currTime = audio.currentTime,
					currMin = parseInt(currTime / 60),
					currSec = parseInt(currTime % 60);
				currSec = (currSec < 10)? '0' + currSec: currSec;	
				currenTimeNode.textContent = currMin + ':' + currSec;

				if(audioDuration > 0) {
					audioPane.style.width = Math.floor((currTime / audioDuration) * CANVAS_WIDTH) + 'px';
					colorLine.style.width = audioPane.style.width;
				} else {
					audioDuration = audio.duration;
				}
			}, false);

			jsAudio.appendChild(audio);

			// Check for non Web Audio API browsers.
			if (!window.AudioContext) {
				//alert("Web Audio isn't available in your browser. But...you can still play the HTML5 audio :)");
				audioPane.className += ' audio__pane__eq_show';
				audioPaneDefault.className += ' audio__pane__eq_show';
				return;
			}

			function rafCallback(time) {
				window.requestAnimationFrame(rafCallback, canvas11);

				var freqByteData = new Uint8Array(analyser.frequencyBinCount);
				analyser.getByteFrequencyData(freqByteData); //analyser.getByteTimeDomainData(freqByteData);

				var SPACER_WIDTH = 1;
				var BAR_WIDTH = 1;
				var OFFSET = (freqByteData.length > CANVAS_WIDTH)? freqByteData.length - CANVAS_WIDTH * 1.5: 100;
				var CUTOFF = 5;

				var numBars = Math.round(CANVAS_WIDTH / SPACER_WIDTH);

				ctx11.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx11.fillStyle = '#F6D565';
				ctx11.lineCap = 'round';
				
				ctx12.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx12.fillStyle = '#F6D565';
				ctx12.lineCap = 'round';

				ctx21.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx21.fillStyle = '#3A5E8C';
				ctx21.lineCap = 'round';

				ctx22.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
				ctx22.fillStyle = '#3A5E8C';
				ctx22.lineCap = 'round';
				
				// Draw rectangle for each frequency bin.
				for (var i = 0; i < numBars; ++i) {
					var magnitude = freqByteData[i + OFFSET] / 3;
					if(i % 2 == 0) {
						ctx11.fillStyle = '#ff7648';
						ctx12.fillStyle = '#ff7648';
						ctx21.fillStyle = '#637a94';
						ctx22.fillStyle = '#637a94';
					} else {
						ctx11.fillStyle = '#ff6633';
						ctx12.fillStyle = '#ff6633';
						ctx21.fillStyle = '#516b88';
						ctx22.fillStyle = '#516b88';
					}
					ctx11.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
					ctx12.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
					ctx21.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
					ctx22.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
				}
			}

			function onLoad(e) {
				setTimeout(function (){
					try {
						window.AudioContext = window.AudioContext||window.webkitAudioContext;
						context = new AudioContext();
						analyser = context.createAnalyser();
					}
					catch(e) {
						// alert('Opps.. Your browser do not support audio API');
					}
					var source = context.createMediaElementSource(audio);
					source.connect(analyser);
					analyser.connect(context.destination);
					audioDuration = audio.duration;
					rafCallback();
				}, 0);
			}

			// Need window.onload to fire first. See crbug.com/112368.
			window.addEventListener('load', onLoad, false);
			
		})();
		/* [/Drawing equalizer] */
		
		function fireEvent(obj,evt){
			var fireOnThis = obj;
			if( document.createEvent ) {
				var evObj = document.createEvent('MouseEvents');
				evObj.initEvent( evt, true, false );
				fireOnThis.dispatchEvent( evObj );
			} else if( document.createEventObject ) {
				var evObj = document.createEventObject();
				fireOnThis.fireEvent( 'on' + evt, evObj );
			}
		}
		
		window.addEventListener('keydown', function(e) {
			if (e.keyCode == 32) { // space
				// Simulate link click on an element.
				fireEvent(window.playButton,'click')
			}
		}, false);

	}

});
