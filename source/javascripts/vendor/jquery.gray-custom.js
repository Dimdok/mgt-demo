/*! Gray v1.4.2 (https://github.com/karlhorky/gray) | MIT */
;(function ($, window, document, undefined) {

  var pluginName = 'gray',
      defaults = {
        fade   : false,
        classes: {
          fade: 'grayscale-fade'
        }
      };

  function Plugin (element, options) {
    var classes,
        fadeClass;

    options = options || {};

    classes = options.classes || {};
    fadeClass = classes.fade || defaults.classes.fade;
    options.fade = options.fade || element.className.indexOf(fadeClass) > -1;

    this.element = element;
    this.settings = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  $.extend(Plugin.prototype, {

    init: function () {
      var element;

      if (!Modernizr.cssfilters &&
        Modernizr.inlinesvg &&
        Modernizr.svgfilters
      ) {
        element = $(this.element);

        if (this.cssFilterDeprecated(element) || this.settings.fade) {
          this.switchImage(element);
        }
      }
    },

    // TODO: Test a freshly made element (modernizr feature test?)
    // instead of testing the active element (fragile)
    cssFilterDeprecated: function(element) {
      return element.css('filter') === 'none';
    },

    elementType: function(element) {
      var type;

      if (element.prop('tagName') === 'IMG') {
        type = 'Img';
      } else {
        type = 'Bg';
      }

      return type;
    },

    getComputedStyle: function(element) {
      var computedStyle = {},
          styles        = {};

      computedStyle = window.getComputedStyle(element, null);

      for(var i = 0, length = computedStyle.length; i < length; i++) {
        var prop = computedStyle[i];
        var val = computedStyle.getPropertyValue(prop);
        styles[prop] = val;
      }

      return styles;
    },

    extractUrl: function(backgroundImage) {
      var url,
          regex;

      startRegex = /^url\(["']?/;
      endRegex   = /["']?\)$/;
      url = backgroundImage.replace(startRegex, '')
                           .replace(endRegex, '');

      return url;
    },

    positionToNegativeMargin: function(backgroundPosition) {
      var x,
          y,
          margin;

      x = backgroundPosition.match(/^(-?\d+\S+)/)[0];
      y = backgroundPosition.match(/\s(-?\d+\S+)$/)[0];

      margin = 'margin:' + y + ' 0 0 ' + x;

      return margin;
    },

    getBgSize: function(url, backgroundSize) {
      var img,
          ratio,
          defaultW,
          w,
          defaultH,
          h,
          size;

      img = new Image();
      img.src = url;

      // TODO: Break this up or simplify
      if (backgroundSize !== 'auto' && backgroundSize !== 'cover' && backgroundSize !== 'contain' && backgroundSize !== 'inherit') {
        var $element = $(this.element);

        ratio    = img.width / img.height;
        w        = parseInt((backgroundSize.match(/^(\d+)px/) || [0,0])[1]);
        h        = parseInt((backgroundSize.match(/\s(\d+)px$/) || [0,0])[1]);
        defaultW = $element.height() * ratio;
        defaultH = $element.width() / ratio;
        w        = w || defaultW;
        h        = h || defaultH;
      }

      if (w || h) {
        size = {
          width: w,
          height: h
        };
      } else {

        size = {
          width : img.width,
          height: img.height
        };
      }

      return size;
    },

    getParams: function(element) {
      var type = this.elementType(element);
      return this['get' + type + 'Params'](element);
    },

    getImgParams: function(element) {
      var params = {};

      params.styles = this.getComputedStyle(element[0]);

      params.svg = {
        url   : element[0].src,
        width : params.styles.width.replace('px', ''),
        height: params.styles.height.replace('px', ''),
        offset: ''
      };

      return params;
    },

    getBgParams: function(element) {
      var params = {},
          url,
          position;

      url       = this.extractUrl(element.css('background-image'));
      bgSize    = this.getBgSize(url, element.css('background-size'));
      offset    = this.positionToNegativeMargin(element.css('background-position'));

      params.styles = this.getComputedStyle(element[0]);

      params.svg = $.extend(
        { url   : url },
        bgSize,
        { offset: offset }
      );

      return params;
    },

    setStyles: function(styles, url, width, height) {
      styles.display  = 'inline-block';
      styles.overflow =
        styles['overflow-x'] =
        styles['overflow-y'] = 'hidden';
      styles['background-image'] = 'url("' + url + '")';
      styles['background-size'] = width + 'px ' + height + 'px';
      delete styles.filter;

      return styles;
    },

    // TODO: Run this outside of the plugin so that it's not run
    // on every element
    addSVGFilterOnce: function() {
      $body = $('body');
      if (!$body.data('plugin_' + pluginName + '_has_filter')) {
        $body.data('plugin_' + pluginName + '_has_filter', 'true')
          .append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="0" height="0" style="position:absolute"><defs><filter id="gray"><feColorMatrix type="saturate" values="0"/></filter></defs></svg>');
      }
    },

    switchImage: function(element) {
      var params,
          classes,
          template;

      params = this.getParams(element);

      classes = this.settings.fade ? this.settings.classes.fade : '';

      template = $(
        '<div class="grayscale grayscale-replaced ' + classes + '">' +
          '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ' + params.svg.width + ' ' + params.svg.height + '" width="' + params.svg.width + '" height="' + params.svg.height + '" style="' + params.svg.offset + '">' +
            '<image filter="url(&quot;#gray&quot;)" x="0" y="0" width="' + params.svg.width + '" height="' + params.svg.height + '" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + params.svg.url + '" />' +
          '</svg>' +
        '</div>');

      params.styles = this.setStyles(params.styles, params.svg.url, params.svg.width, params.svg.height);

      // TODO: Should this really set all params or should we set only unique ones by comparing to a control element?
      template.css(params.styles);

      this.addSVGFilterOnce();
      element.replaceWith(template);
    }
  });

  $.fn[pluginName] = function (options) {
    this.each(function() {
      if (!$.data(this, 'plugin_' + pluginName)) {
        $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
      }
    });
    return this;
  };

  $(window).on('load', function() {
    $('.grayscale:not(.grayscale-replaced)')[pluginName]();
  });

})(jQuery, window, document);
