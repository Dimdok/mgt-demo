
$(function() {

	window.App = window.App || {};

	App.Manage = (function() {
			
		$(document).on('ifToggled', '.js-check-all', function() {
			var $scope = $(this).closest('.js-manage-parent');
			var $button = $(this);
			toggleItems($scope, $button);
			updateButtons($scope);
		});

		$(document).on('ifToggled', '.js-check-all-area :checkbox', function() {
			var $scope = $(this).closest('.js-manage-parent');
			updateCtrls($scope);
		});
		
		// check/uncheck all
		function toggleItems($scope, $button) {
			var $items = $scope.find('.js-check-all-area :checkbox');
			var checked = $button.prop('checked');

			$items
				.prop('checked', checked)
				.iCheck('update');
				
			App.Forms.updateIChecksParent($items);
		}

		// update all buttons
		function updateCtrls($scope) {
			var $checkAll = $scope.find('.js-check-all');
			var $items = $scope.find('.js-check-all-area :checkbox');
			var counts = getCountInfo($items);

			if (isAllChecked(counts)) {
				$checkAll
					.prop('checked', true)
					.iCheck('update');
			} else {
				$checkAll
					.prop('checked', false)
					.iCheck('update');
			}

			updateButtons($scope);
		}

		// update action buttons only
		function updateButtons($scope) {
			var $perOneButtons = $scope.find('.js-manage-button[data-target="one"]');
			var $perAllButtons = $scope.find('.js-manage-button[data-target="all"]');
			var $items = $scope.find('.js-check-all-area :checkbox');
			var counts = getCountInfo($items);

			if (isOneChecked(counts)) {
				$perAllButtons.removeClass('hidden');
				$perOneButtons.removeClass('hidden');
			} else if (isAnyChecked(counts)) {
				$perAllButtons.removeClass('hidden');
				$perOneButtons.addClass('hidden');
			} else {
				$perAllButtons.addClass('hidden');
				$perOneButtons.addClass('hidden');
			}
		}

		function getCountInfo($items) {
			var totalCount = $items.size();
			var $checked = $items.filter(':checked');
			var checkedCount = $checked.size();

			return {
				'total': totalCount,
				'checked': checkedCount
			};
		}

		function isOneChecked(counts) {
			return counts.checked == 1; 
		}

		function isAnyChecked(counts) {
			return counts.checked > 0;
		}

		function isAllChecked(counts) {
			return counts.total == counts.checked;
		}

		return {
			update: function() {

				$('.js-manage-parent').each(function() {
					var $scope = $(this);
					updateCtrls($scope);
				});

			}
		}

	}());

}());